import { Injectable , EventEmitter } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  
  private display: boolean;
  public loading_occured    = new EventEmitter<boolean>();


  private emit(): void {
    this.loading_occured.emit( this.display );
  }

  public hide(): void {
    this.display = false;
    this.emit();
  }

  public show(): void {
      this.display = true;
      this.emit();
  }

}
