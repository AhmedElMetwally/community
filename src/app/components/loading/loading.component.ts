import { LoadingService    } from './../../service/loading.service';
import { Component, OnInit } from '@angular/core';

declare let $;


@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {

  constructor(
    private loadingService: LoadingService
  ) { }

  public displayLoading: boolean;


  private setOptions() {
    this.displayLoading = false;
  }

  public initLoading(): void {
    this.loadingService.loading_occured.subscribe( 
      displayLoading => this.displayLoading = displayLoading
    );
  }

  public ngOnInit() {
    this.setOptions();
    this.initLoading()
  }

}
