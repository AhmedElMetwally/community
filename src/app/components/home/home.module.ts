import { HomeService } from './../../service/home.service';
import { HomeComponent } from './home.component';
import { NgModule      } from '@angular/core';
import { CommonModule  } from '@angular/common';
import { TabViewModule } from 'primeng/tabview';
import { ChartModule   } from 'primeng/chart';

import { HomeRoutingModule } from './home-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    ChartModule,
    TabViewModule
  ],
  declarations: [
    HomeComponent
  ],
  providers : [
    HomeService
  ]
})
export class HomeModule { }
