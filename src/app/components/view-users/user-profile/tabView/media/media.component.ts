import { ActivatedRoute    } from '@angular/router';
import { environment       } from './../../../../../../environments/environment';
import { ErrorService      } from './../../../../../service/error.service';
import { UsersService      } from './../../../../../service/user.service';
import { LoadingService    } from './../../../../../service/loading.service';
import { OnInit, Component } from '@angular/core';
import { Title             } from '@angular/platform-browser';

declare let $ , swal;

@Component({
  selector: 'app-user-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent implements OnInit {

  constructor(
    private titleService:    Title,
    private loadingService:  LoadingService,
    private usersService:    UsersService,
    private errorService:    ErrorService,
    private route:           ActivatedRoute,
    

) { }

private id: string;
private userToken: string;

public medias: any[];

public userUrl:  string;

private page:    number;
private limit:   number;

public loading: boolean;

public mediasLength: number;

public lockScroll: boolean;



private setMedias( medias: any[] ): void {
  let len = medias.length;
  this.lockScroll = len !== this.limit
  this.loading = false;
  this.medias = [...this.medias , ...medias];
  this.mediasLength += len;
} 

public onScroll(): void {

  if( this.lockScroll ) return;

  this.loading = true;

  this.usersService.getStatuses( this.page , this.limit  , false  , 'media', this.id , this.userToken)
  .then(({ data }) => this.setMedias( data ) )
  .catch((e) => this.errorService.showError( e ) );
  

  this.page += 1;
  this.lockScroll = true;
  
}

private reset(): void {
  this.page = 1;
  this.limit = 16;
  
  this.medias = [];
  this.mediasLength = 0;
  this.lockScroll = false;
  
  setTimeout(() => {
    this.onScroll();
  } , 100 );
}

private getParams(): Promise<any> {
  return new Promise( resolve => {
    this.route
      .params
      .subscribe( params => {
        this.id = params.id;
        resolve()
      })
  })
}

private getUser(): Promise<any>  {
  return new Promise( resolve => {

    this.loadingService.show();

    this.usersService.getUser( this.id )
    .then(({ data }) => {
      this.userToken = data.token;
      this.loadingService.hide();
      resolve();
    })
    .catch( e => this.errorService.showError( e ));

  })

}

public deleteMedia( id ): void {
  swal({
    title: "Are you sure?",
    text: "You Will Delete This Media",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {

    if (! willDelete) return  swal("Media Is Safe!");

    this.loadingService.show();
  
    this.usersService.deleteStatuse( id  , this.userToken )
    .then(( res ) => {

      this.medias = this.medias.filter( v => {
        return v.id !== id 
      });          

      this.mediasLength -= 1;

      swal("Media Deleted", { icon: "success" });

      this.loadingService.hide();

    })
    .catch( error => this.errorService.showError( error ) )

  });
  
}

private setOptions(): Promise<any> {
  return new Promise( resolve => {

    this.loading = true;

    this.userUrl = environment.userUrl;

    resolve();

  })
}

public scrollToTop(): void {
  $("html, body").animate({ scrollTop: $('.__view_user').height() }, 600);
}

private jquery(): void {
  setTimeout(() => {
    
    const scrollFunc = () => {
      if( $(window).scrollTop() < $('.__view_user').height() ) {
        $('.scroll-top-btn').removeClass('scroll-top-btn-show').addClass('scroll-top-btn-hide');
      } else {
        $('.scroll-top-btn').removeClass('scroll-top-btn-hide').addClass('scroll-top-btn-show');
      }
    }

    $(window).scroll( scrollFunc )


  } , 250)
}

public async ngOnInit() {
  this.jquery();
  await this.getParams();
  this.getUser()
  .then(() => {
    setTimeout(() => {
      this.reset();
    } , 250 ); 
  });
  await this.setOptions();
  
}


}






 