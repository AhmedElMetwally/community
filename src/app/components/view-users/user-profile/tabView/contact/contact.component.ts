import { environment } from './../../../../../../environments/environment';
import { LoadingService    } from './../../../../../service/loading.service';
import { ActivatedRoute    } from '@angular/router';
import { UsersService      } from './../../../../../service/user.service';
import { ErrorService      } from './../../../../../service/error.service';
import { Component, OnInit } from '@angular/core';

declare let swal , $;

@Component({
  selector: 'app-user-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {


  constructor(
    private errorService:   ErrorService,
    private route :         ActivatedRoute,
    private usersService:   UsersService,
    private loadingService: LoadingService,
  ) { };

  public contacts:  any[];
  public id:        string;
  public userToken: string;
  
  public userUrl: string;

  private getParams(): Promise<any> {
    return new Promise( resolve => {
      this.route
        .params
        .subscribe( params => {
          this.id = params.id;
          resolve()
        })
    })
  }

  private getUserContacts(): void  {
    this.loadingService.show();

    this.usersService.getUser( this.id )
    .then(({ data }) => {
      this.userToken = data.token;

      this.usersService.getContacts( this.userToken )
      .then( ({ data }) => {
        this.loadingService.hide();
        this.contacts = data;
      })
      .catch( e => this.errorService.showError( e ));

    })
    .catch( e => this.errorService.showError( e ));

  }

  public deleteContact( contactName , mobileNumber ): void {
    const obj = {
      contactName , mobileNumber 
    };
    swal({
      title: "Are you sure?",
      text: "You Will Delete This Contact",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (! willDelete) return  swal("Contact Is Safe!");

        this.loadingService.show();

        this.usersService.deleteContact( obj , this.userToken )
        .then(( res ) => {
          this.contacts = this.contacts.filter( v => {
            return v.contactName !== contactName && v.mobileNumber !== mobileNumber; 
          });          
          swal("Contact Deleted", {
            icon: "success",
          });
          this.loadingService.hide();
        })
        .catch( error => this.errorService.showError( error ) )

    });
    

    console.log(obj)
  }

  public scrollToTop(): void {
    $("html, body").animate({ scrollTop: $('.__view_user').height() }, 600);
  }

  private jquery(): void {
    setTimeout(() => {
      
      const scrollFunc = () => {
        if( $(window).scrollTop() < $('.__view_user').height() ) {
          $('.scroll-top-btn').removeClass('scroll-top-btn-show').addClass('scroll-top-btn-hide');
        } else {
          $('.scroll-top-btn').removeClass('scroll-top-btn-hide').addClass('scroll-top-btn-show');
        }
      }

      $(window).scroll( scrollFunc )


    } , 250)
  }

  public async ngOnInit() {
    await this.getParams();
    await this.getUserContacts();
    this.jquery();

    this.userUrl = environment.userUrl;
  }

}
