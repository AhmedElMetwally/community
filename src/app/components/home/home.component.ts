import { ErrorService } from './../../service/error.service';
import { HomeService       } from './../../service/home.service';
import { LoadingService    } from './../../service/loading.service';
import { Component, OnInit } from '@angular/core';
import { Title             } from '@angular/platform-browser';
import { ActivatedRoute    } from '@angular/router';


declare const Chart: any , $: any;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private route:          ActivatedRoute, 
    private titleService:   Title, 
    private loadingService: LoadingService ,
    private homeService:    HomeService ,
    private errorService:   ErrorService
  ) { };
  
  public disLikesData:  any ;
  public likesData:     any ;
  public usersData:     any ;
  public postsData:     any ;

  public allUsers:    number;
  public allLikes:    number;
  public allDisLikes: number;
  public allPosts:    number;

  public chartOptions: any ;

  

  private setOptions(): void {
    this.chartOptions = {
      legend: {display: false}
    }
  };

  private getData(): void {

    this.loadingService.show();
    this.homeService.getData()
    .then( ({ data }) => {

      let likes      = new Array(12).fill(0); 
      let disLikes   = new Array(12).fill(0); 
      let users      = new Array(12).fill(0); 
      let posts      = new Array(12).fill(0); 

      data.forEach( v => {
        likes[ parseInt( v.currentMonth) ]    =  parseInt( v.likes || 0 );
        disLikes[ parseInt( v.currentMonth )] =  parseInt( v.dislikes || 0 );
        users[ parseInt( v.currentMonth) ]    =  parseInt( v.users || 0 );
        posts[ parseInt( v.currentMonth) ]    =  parseInt( v.posts || 0 );
      });

      this.allLikes = likes.reduce( (a , b ) => {
        return a + b
      } , 0);

      this.allDisLikes = disLikes.reduce( (a , b ) => {
        return a + b
      } , 0)

      this.allPosts = posts.reduce( (a , b ) => {
        return a + b
      } , 0)

      this.allUsers = users.reduce( (a , b ) => {
        return a + b
      } , 0)


      this.disLikesData = {
      
        labels: [
          'January', 'February', 'March', 'April', 
          'May', 'June', 'July' , 'August' , 
          'September' , 'October' ,'November' , 'December'
        ],
  
        datasets: [
            {
              label: 'DisLikes',
              backgroundColor: 'gray',
              borderColor: '#3e3d3d',
              data: disLikes
            }
        ]
      };
  
      this.likesData = {
        
        labels: [
          'January', 'February', 'March', 'April', 
          'May', 'June', 'July' , 'August' , 
          'September' , 'October' ,'November' , 'December'
        ],
  
        datasets: [
            {
              label: 'Likes',
              backgroundColor: '#36A2EB',
              borderColor: '#1E88E5',
              data: likes
            }
        ]
      };
  
      this.usersData = {
        
        labels: [
          'January', 'February', 'March', 'April', 
          'May', 'June', 'July' , 'August' , 
          'September' , 'October' ,'November' , 'December'
        ],
  
        datasets: [
            {
              label: 'users',
              backgroundColor: '#407ffc',
              borderColor: '#1e65f3',
              data: users
            }
        ]
      };
  
      this.postsData = {
        
        labels: [
          'January', 'February', 'March', 'April', 
          'May', 'June', 'July' , 'August' , 
          'September' , 'October' ,'November' , 'December'
        ],
  
        datasets: [
          {
              label: 'Posts',
              backgroundColor: '#f39c12',
              borderColor: '#c7851c',
              data: posts
          }
        ]
      };

      this.loadingService.hide();

    })
    .catch( e => this.errorService.showError(e));

  }
  
  private setTitle( title: string ): void {
    this.titleService.setTitle( title );
  }

  public ngOnInit(): void  {
    this.setTitle('Home');
    this.setOptions();
    this.getData()
  }

}
