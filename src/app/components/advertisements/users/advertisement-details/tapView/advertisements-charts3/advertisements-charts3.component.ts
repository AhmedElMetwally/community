import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-advertisements-charts3',
  templateUrl: './advertisements-charts3.component.html',
  styleUrls: ['./advertisements-charts3.component.css']
})
export class UsersAdvertisementsCharts3Component implements OnInit {


  public Data: any;

  // min & max dates 
  public minDate:any;
  public maxDate:any;

  // ranbe model
  public rangeDates: any;


  public chartOptions: any ;


  private setOptions(): void {
    this.chartOptions = {
      legend: {display: false}
    }
  };

  constructor(

  ) { 

    this.Data = {
      
      labels: [
        'Muslim', 'Christian', 'Jewish', 'other', 
      ],
  
      datasets: [
          {
              label: 'Users',
              borderColor: '#6ace1e',
              backgroundColor: '#6edd19',
              fill: false,
              data: [
                70,
                55, 
                75, 
                40, 
              ]
          },
      ]
    };

  }


  ngOnInit() {
    this.minDate = new Date('2018/05/02')
    this.maxDate = new Date('2018/05/22')

    this.setOptions();
  }

}
