import { EmployeesAdvertisementsComponent       } from './employees/advertisements.component';
import { AddEmployeeAdvertisementComponent      } from './employees/add-advertisement/add-advertisement.component';
import { EmployeesAdvertisementDetailsComponent } from './employees/advertisement-details/advertisement-details.component';
import { AdvertisementsMapComponent             } from './map/advertisements-map.component';

import { UsersAdvertisementsComponent       } from './users/advertisements.component';
import { UsersAdvertisementDetailsComponent } from './users/advertisement-details/advertisement-details.component';


import { Routes, RouterModule } from '@angular/router';
import { NgModule             } from '@angular/core';


const routes: Routes = [
  {
    path : 'users',
    component : UsersAdvertisementsComponent
  },
  {
    path : 'users/:id',
    component : UsersAdvertisementDetailsComponent
  },



  {
    path : 'employees',
    component : EmployeesAdvertisementsComponent
  },
  {
    path: 'employees/add',
    component : AddEmployeeAdvertisementComponent
  },
  {
    path : 'employees/:id',
    component : EmployeesAdvertisementDetailsComponent
  },


  {
    path: 'map',
    component : AdvertisementsMapComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdvertisementsRoutingModule { }
