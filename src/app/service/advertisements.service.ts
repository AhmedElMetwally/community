import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AdvertisementsService {

  constructor(
    private http: HttpClient
  ) { }

  public getLocationByCode( code: string): Promise<any> {
    return this.http.get(
      'https://restcountries.eu/rest/v2/alpha/' + code,
    ).toPromise()
  }
  
}
