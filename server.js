var express = require('express');
var path = require('path');


var app = express();
app.listen(3000 , () =>{
    console.log(' server run in')
    console.log('    http://localhost:3000')
    console.log(' or')
    console.log('    http://173.255.203.97:3000')
});

app.use(express.static(path.join(__dirname, '/dist/community')));

app.get('/*',function(req, res, next) {
    res.sendFile(path.join(__dirname, '/dist/community/index.html'));
});

module.exports = app;

 