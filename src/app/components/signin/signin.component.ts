import { FormGroup , Validators , FormControl } from '@angular/forms';

import { LoadingService } from './../../service/loading.service';
import { AuthService    } from './../../service/auth.service';
import { Title          } from '@angular/platform-browser';

import { Component, OnInit      } from '@angular/core';
import { Router ,ActivatedRoute } from '@angular/router';
import { JwtHelperService       } from '@auth0/angular-jwt';

import { ErrorService           } from './../../service/error.service';

declare const $: any , swal: any;

const helper = new JwtHelperService();


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(
    private router:         Router, 
    private route:          ActivatedRoute, 
    private errorService:   ErrorService, 
    private authService:    AuthService,
    private titleService:   Title , 
    private loadingService: LoadingService,
  ) { }

  public signinForm: FormGroup;


  private toTitleCase( str: string): string {
    let split =  str.replace( /[A-Z]/g , function( s: string ): any {
      return ' ' + s.toLowerCase() 
    });
    return split.replace(/\w\S*/g, function(txt: string){
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    })
  }

  private initForm(): void {

    const checkPatternEmail = (c: FormControl) => /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test( c.value ) ? null : {
      invalid: true
    };

    const checkLengthPassword = (c: FormControl) => /.{6,30}/g.test( c.value ) ? null : {
      invalid: true
    };

    this.signinForm =  new FormGroup({ 
      email    : new FormControl('admin@admin.com' , [ Validators.required ,  checkPatternEmail ]),
      password : new FormControl('123456', [Validators.required , checkLengthPassword ])
    });

  }

  private setTitle(): void {
    this.titleService.setTitle( "Signin" );
  };

  private jquery(): void {
    $('.js-tilt').tilt({
			scale: 1.1
    });
  };

  private goToHome(): void{
    this.router.navigate(['/home'])
  }

  private checkIfAdmin(): void{
    if ( this.authService.isSignin() ) {
      this.goToHome();
    } 
  }

  private saveToken( token: string): void {
    localStorage.setItem('token' , token);
  }

  public signout(): void {
    localStorage.clear();
    this.router.navigate(['/signin'])
  }

  public saveEmployee( token ) {
    localStorage.setItem('employee' , JSON.stringify(
      helper.decodeToken( token )
    ))
  }

  public onSubmit(): void{

    if( this.signinForm.invalid) {
      for(let control in this.signinForm.controls ) {

        if( this.signinForm.controls[control].invalid ) {

          let _control = this.toTitleCase( control );
          if( this.signinForm.controls[control].errors.required ) {
            swal( _control , `${_control} Is Required`  , "error");
          } else if ( this.signinForm.controls[control].errors.invalid ) {
            swal( _control , `${_control} Is Invalid`  , "error");
          } 
          break;
          
        }

      }
      return
    }

    let email    = this.signinForm.controls.email.value;
    let password = this.signinForm.controls.password.value;

    this.loadingService.show();
    
    this.authService.signin( email , password )
    .then( res => {

      localStorage.clear();

      this.saveEmployee( res.data.token )

      this.saveToken( res.data.token );
      this.goToHome();

      this.authService.connetSocketIo().subscribe( 
        () => this.authService.loginWithSocketIo() ,
        () => this.signout() 
      );

    })
    .catch( err => this.errorService.showError( err) );


  };

  public getErrorMsg( obj ): string {
    if(obj){
      if( obj.required) {
        return 'Required'
      } else if ( obj.invalid ){
        return 'Invalid'
      }
    }
  }

  public ngOnInit() {
    this.initForm();
    this.checkIfAdmin();
    this.setTitle();
    this.jquery();
  }

}
