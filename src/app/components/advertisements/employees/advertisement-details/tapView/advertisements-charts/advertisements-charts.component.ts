import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employees-advertisements-charts',
  templateUrl: './advertisements-charts.component.html',
  styleUrls: ['./advertisements-charts.component.css']
})
export class EmployeesAdvertisementsChartsComponent implements OnInit {


  public Data: any;

  // min & max dates 
  public minDate:any;
  public maxDate:any;

  // ranbe model
  public rangeDates: any;



  public chartOptions: any ;


  private setOptions(): void {
    this.chartOptions = {
      legend: {display: false}
    }
  };

  constructor(

  ) { 

    this.Data = {
      
      labels: [
        '2018.05.02', '2018.05.03', '2018.05.04', '2018.05.05', 
        '2018.05.06', '2018.05.07', '2018.05.07' , '2018.05.08' , 
        '2018.05.09' , '2018.05.10' ,'2018.05.11' , '2018.05.12',
        '2018.05.13' ,'2018.05.14','2018.05.15','2018.05.16',
        '2018.05.17','2018.05.18','2018.05.19','2018.05.20','2018.05.21',
        '2018.05.22'
      ],
  
      datasets: [
          {
              label: 'Click',
              borderColor: '#36A2EB',
              backgroundColor: '#1f88e5',
              fill: false,
              data: [
                50,
                125, 
                49, 
                80, 
  
                65, 
                71, 
                55, 
                105,
  
                125, 
                115, 
                165, 
                145, 

                65, 
                71, 
                55, 
                105,
  
                125, 
                115, 
                165, 
                145, 

                44,
                66
              ]
          },
          {
            label: 'View',
            fill: false,
            borderColor: '#6ace1e',
            backgroundColor: '#6edd19',
            data: [
             
              65, 
              71, 
              55, 
              105,

              125, 
              115, 
              165, 
              145,  

              65, 
              71, 
              55, 
              105,


              50,
              125, 
              49, 
              80, 

             
              44,
              66,

              125, 
              115, 
              165, 
              145, 

             
            ]
        }
      ]
    };

  }


  ngOnInit() {
    this.minDate = new Date('2018/05/02');
    this.maxDate = new Date('2018/05/22');

    this.setOptions();
  }

}
