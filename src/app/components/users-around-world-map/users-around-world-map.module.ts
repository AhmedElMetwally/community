import { UsersService } from './../../service/user.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TabViewModule } from 'primeng/tabview';
import { UsersAroundWorldMapRoutingModule } from './users-around-world-map-routing.module';
import { UsersAroundWorldMapComponent } from './users-around-world-map.component';
import { FormsModule } from '@angular/forms';
import { SliderModule } from 'primeng/slider';
import {ChartModule} from 'primeng/chart';
import { DropdownModule } from 'primeng/dropdown';
import {DataScrollerModule} from 'primeng/datascroller';
import { ButtonModule } from 'primeng/button';
import {GMapModule} from 'primeng/gmap';

@NgModule({
  imports: [
    CommonModule,
    UsersAroundWorldMapRoutingModule,
    AutoCompleteModule,
    TabViewModule,
    FormsModule,
    SliderModule,
    ChartModule,
    DropdownModule,
    GMapModule,
    DataScrollerModule ,
    ButtonModule   
  ],
  declarations: [
    UsersAroundWorldMapComponent,
  ],
  providers : [
    UsersService
  ]
})
export class UsersAroundWorldMapModule { }
