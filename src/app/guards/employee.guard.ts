import { AuthService } from './../service/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate , ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
  providedIn : 'root'
})
export class isEmployeeGuard implements CanActivate {
  constructor(
    private authService: AuthService ,
    private router:Router
  ){ }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ):  boolean {
    if( ! this.authService.isSignin()) {
      this.router.navigate(['/signin']);
      return false
    }
    return true
  }
}

@Injectable({
  providedIn : 'root'
})
export class isNotEmployeeGuard implements CanActivate {
  constructor(
    private authService: AuthService ,
    private router:Router
  ){ }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ):  boolean {
    if( this.authService.isSignin() ) {
      this.router.navigate(['/home'])
      return false
    }
    return true;
  }
}


@Injectable({
  providedIn : 'root'
})
export class isHaveRoleGuard implements CanActivate {
  constructor(
    private authService: AuthService ,
    private router:      Router
  ){ }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const groups     = route.data.groups as string[];

    if(! this.authService.checkGroups( groups  ) ) {
      this.router.navigate(['/home'])
      return false
    }
    return true;
  }
}



