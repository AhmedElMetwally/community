import { DataScrollerModule } from 'primeng/datascroller';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ViewUsersComponent   } from './view-users.component';
import { LocationComponent    } from './user-profile/tabView/location/location.component';
import { ContactComponent     } from './user-profile/tabView/contact/contact.component';
import { MediaComponent } from './user-profile/tabView/media/media.component';
import { ChartComponent } from './user-profile/tabView/chart/chart.component';
import { PostsComponent } from './user-profile/tabView/posts/posts.component';
import { StoryComponent } from './user-profile/tabView/story/story.component';

import { ViewUsersRoutingModule } from './view-users-routing.module';
import { UsersService           } from '../../service/user.service';
import { InfiniteScrollModule   } from "ngx-infinite-scroll";

import { NgModule     } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule  } from '@angular/forms';

import { DataViewModule  } from 'primeng/dataview';
import { PanelModule     } from 'primeng/panel';
import { DropdownModule  } from 'primeng/dropdown';
import { TabViewModule   } from 'primeng/tabview';
import { ButtonModule    } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { DataGridModule  } from 'primeng/datagrid';
import { ChartModule     } from 'primeng/chart';
import { DataListModule  } from 'primeng/datalist';
import { TooltipModule   } from 'primeng/tooltip';
import { GMapModule      } from 'primeng/gmap';
import { LightboxModule  } from 'primeng/lightbox';
import { CalendarModule  } from 'primeng/calendar';
import { InputMaskModule } from 'primeng/inputmask';
import { FileUploadModule } from 'primeng/fileupload';
import { TimeAgoPipe     } from 'time-ago-pipe';


@NgModule({
  imports: [
    CommonModule,
    ViewUsersRoutingModule,
    DataViewModule,
    FormsModule,
    PanelModule,
    DropdownModule,
    TabViewModule,
    InputTextModule,
    ButtonModule,
    DataGridModule,
    ChartModule,
    DataListModule,
    TooltipModule,
    GMapModule,
    InputMaskModule,
    FileUploadModule,
    InfiniteScrollModule,

    DataScrollerModule
  ],
  declarations: [
    ViewUsersComponent,
    UserProfileComponent,
    MediaComponent,
    ChartComponent,
    ContactComponent,
    LocationComponent,
    PostsComponent,
    StoryComponent,
    TimeAgoPipe
  ],
  providers : [
    UsersService
  ]
})
export class ViewUsersModule { }
