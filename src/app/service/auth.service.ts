import { Observable } from 'rxjs';
import { Injectable  } from '@angular/core';
import { Router      } from '@angular/router';
import { environment } from '../../environments/environment';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

declare let io;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  public url:    string = environment.url
  public socket: any;

  private _headers:HttpHeaders = new HttpHeaders();
  private headers = this._headers
      .append('Content-Type', 'application/json')
      .append('Authorization', 'Bearer ' + localStorage.getItem('token'))
  ;

  public connetSocketIo(): Observable<any> {
    this.socket = io( this.url , {
      transports: [ 'websocket']
    });
    
    return new Observable( observable  => {

      this.socket.on('connect', () => { 
        observable.next(); 
      });

      this.socket.on('disconnect', () => { 
        observable.error(); 
        console.log('disconnect'); 
      });

    });
  }

  public loginWithSocketIo(): void {
    this.socket.emit('login' , {
      token : localStorage.getItem('token')
    });

    this.socket.on('login' , res => {
      console.log("Connect To Socket.io Done")
    });
    
  }
  
  public isSignin(): boolean {
    return localStorage.getItem('token') !== null;
  }

  public signin( email: string , password: string): Promise<any> {
    return this.http.post(
      `${environment.url}/api/v1/auth/login`,
      {
        email,
        password
      }
    ).toPromise()
  }

  public refreshToken(): Promise<any> {
    return this.http.post(
      `${environment.url}/api/v1/auth/token`,
      null,
      {
        headers : this.headers
      }
    ).toPromise()
  }


  public checkGroups( groupsData ): boolean {

    if( localStorage.getItem('employee') == null ) return false;

    const employeeGroups = JSON.parse(
      localStorage.getItem('employee')
    ).groups as any;

    for(let i of  groupsData ){
      for(let k of  employeeGroups ){
        if( i == k ) return true;
      }
    }
    return false

  }

  public isSuperEmployee() : boolean {

    if( localStorage.getItem('employee') == null ) return false;

    const employeeRoles = JSON.parse(
      localStorage.getItem('employee')
    ).roles as any;
    
    return employeeRoles.indexOf('SUPER_EMPL') !== -1

  }
  
  
}
