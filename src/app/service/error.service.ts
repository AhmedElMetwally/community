import { LoadingService } from './loading.service';
import { Injectable     } from '@angular/core';
declare let swal;

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(
    private loadingService: LoadingService
  ) { }


  public showError( error ) {
    console.log(error);
    this.loadingService.hide();
    swal( `Try Again` , '' , "error");
  }

  
}
