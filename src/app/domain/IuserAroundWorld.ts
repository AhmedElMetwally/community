export interface IuserAroundWorld {
    code: string;
    name: string;
    value: number;
    color: string;
}
