import { Location          } from '@angular/common';
import { AuthService       } from './service/auth.service';
import { LoadingService    } from './service/loading.service';
import { Component, OnInit } from '@angular/core';
import { JwtHelperService  } from '@auth0/angular-jwt';

import { Router, NavigationStart, Event, NavigationEnd, NavigationCancel, NavigationError, RoutesRecognized } from '@angular/router';

const helper = new JwtHelperService();
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  constructor(
    private authService:     AuthService,
    private loadingService:  LoadingService,
    private router:          Router,
    private location:        Location,
  ){}
  

  public isSuperEmployee(){
    return this.authService.isSuperEmployee();
  }

  public checkGroups(groupsData): boolean {
    return this.authService.checkGroups( groupsData );
  }

  public isSignin(): boolean {
    return this.authService.isSignin();
  }

  public signout(): void {
    localStorage.clear();
    this.router.navigate(['/signin'])
  }

  public back(): void {
    this.location.back();
  }

  public saveEmployee( token ) {
    localStorage.setItem('employee' , JSON.stringify(
      helper.decodeToken( token )
    ))
  }

  private refreshToken( token: string ): void {
    localStorage.setItem('token' , token);
    this.saveEmployee( token );
  }

  public ngOnInit(): void {
    
    
    this.router.events.subscribe( (event: Event) => {

      if( event instanceof NavigationStart ) {
        this.loadingService.show()
      };

      if( event instanceof NavigationEnd ) {
        this.loadingService.hide()
      };

    });


    if( this.isSignin() ) {

      this.authService.refreshToken()
      .then( res => {
        this.refreshToken( res.data.token );
        
        this.authService.connetSocketIo().subscribe( 

          () => this.authService.loginWithSocketIo() ,
          () => this.signout() 

        );

      })
      .catch( err => this.signout() )

    }



  }

}
