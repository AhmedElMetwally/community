import { ErrorService    } from './../../service/error.service';
import { UsersService    } from './../../service/user.service';
import { Title           } from '@angular/platform-browser';
import { ActivatedRoute  } from '@angular/router';
import { LoadingService  } from './../../service/loading.service';
import { environment     } from '../../../environments/environment';

import { Component, OnInit, ViewChild } from '@angular/core';

import { IuserAroundWorld   } from './../../domain/IuserAroundWorld';
import { latLong            } from './countriescode-latlong';
import { CountriesCode_Name } from './countriescode-name';

declare const AmCharts: any , $: any  ;


type IuserStatics = {
  name:   string;
  code:   string;
  posts5: number;
  totalOnline:  number;
  totalUsers:   number;
  totalOffline: number
  dialCode?:    string;

  percent?:     number;
}


@Component({
  selector: 'app-users-around-world-map',
  templateUrl: './users-around-world-map.component.html',
  styleUrls: ['./users-around-world-map.component.css']
})
export class UsersAroundWorldMapComponent implements OnInit {
  
  constructor(
    private usersService:   UsersService,    
    private loadingService: LoadingService,
    private titleService:   Title, 
    private errorService:   ErrorService
  ) { }


  private countriesHaveUsers: string[]; // list of country code

  private allUsers: any[];

  // map function
  public map: any;
  // data in map images
  private dataProviderMap: any;

  
  // user country from DB
  private userStatics: IuserStatics[];
  private userStaticsHalfLength: number;

  // auto complate filter data
  SearchFilteredCountries: {
    name: string;
    code: string;
  }[];
  // auto complate module
  autoComplete: {
    name: string;
    code: string;
  }|any;


  // show to hide details in map
  public mapDialogBoxType: 'country'|'user' ;

  // content of country details in map
  public selectedCountryDetails: IuserStatics;

  public selectedUserDetails: any;


  public userUrl: string

  // close country details in map on click home of map 
  private jquery(): void {
    $('body').on( 'click' , '.amcharts-pan-home' , ()=>{
      this.closeDetails();
    });
  }

  private closeDetails(): void {
      this.mapDialogBoxType = null;
  }  

  // country details in map
  private setCountryDetails( code: string ): void {

    let isInUserStatics = false;
    
    for( let c of this.userStatics ) {
      if( c.code == code && c.totalUsers != 0  ) {
        isInUserStatics = true;

        this.selectedCountryDetails = {
          name : c.name,
          code : c.code,
          totalUsers   : c.totalUsers,
          totalOnline  : c.totalOnline,
          totalOffline : c.totalUsers - c.totalOnline,
          posts5  : c.posts5 ,
          percent : parseInt(c.totalOnline/c.totalUsers * 100 + ''),
        }
        
        break;
      }
    }

    if(isInUserStatics){
      this.mapDialogBoxType = 'country';
    } else {
      this.closeDetails();
    }

  }

  // user details in map
  private setUserDetails( user_id: string ): void {
     
    this.mapDialogBoxType = null;
    this.loadingService.show()

    this.usersService.getUser( user_id )
    .then(({ data }) => {
      this.selectedUserDetails = data.result;
      this.mapDialogBoxType = 'user';
      this.loadingService.hide()
    })
    . catch( error => this.errorService.showError(error))
    
  }

  // auto complate 
  public get autoCompleteEvents(): any {
    const events: any = {};
    events.onSelect = (event): void => {
      this.map.selectObject();
      
      this.setCountryDetails( event.code );
      this.setUsersInDataProviderImages( this.map.getObjectById(event.code) , event.code );
    };

    events.onClear = (): void => {
      this.closeDetails();
      this.map.selectObject();
    };

    events.completeMethod = ( event ): void => {
      this.SearchFilteredCountries = [];
      for(let i = 0, len = CountriesCode_Name.length ; i < len ; i++) {
          let CountryCode_Name = CountriesCode_Name[i];
          if( CountryCode_Name.name.toLowerCase().indexOf(event.query.toLowerCase()) == 0 ) {
              this.SearchFilteredCountries.push(CountryCode_Name);
          }
      }
    }
    return events;
  }

  public setUsersInDataProviderImages( selectedMapObject: any , code: string) {
    
    if( this.countriesHaveUsers.indexOf(code.toUpperCase()) >= 0 ) return this.map.selectObject(  selectedMapObject );
    
    this.loadingService.show();
    this.countriesHaveUsers.push(code.toUpperCase());

    this.usersService.getUsers( code.toUpperCase() , 1 , Number.MAX_SAFE_INTEGER , 'all' )
    .then( ({data}) => {

      this.allUsers = [...this.allUsers , ...data ];
      
      for( let user of data ) {
        
        let [ lat , long ] = user.location.split(',');

        this.dataProviderMap.images.push({
          "id"            : code.toUpperCase() ,
          "user_id"       : user.id,
          "mobileNumber"  : user.mobileNumber,
          "type"          : 'circle',
          "width"         : 10,
          "height"        : 10,
          "color"         : '#2499e5',
          "longitude"     : long ,
          "latitude"      : lat   ,
          "title"         : user.firstName + ' ' + user.lastName,
          "labelFontSize" : "11",
          "labelColor"    : "white",
          "selectedLabelColor" : "white",
          "labelRollOverColor" : "white",
          "zoomLevel"     : 3.3,
        });
      }

      this.map.validateData();
      this.map.selectObject(  selectedMapObject );
      
      setTimeout(() => {
        this.loadingService.hide();
      } , 1000 )

    })
    . catch( error => this.errorService.showError(error))

  }

  private getUserStatics(): Promise<any> {
    return new Promise( resolve => {

      this.loadingService.show();

      this.usersService.getUserStatics()
      .then( ({data}) => {
        this.userStatics = data;
        this.loadingService.hide();
        return resolve()
      })
    . catch( error => this.errorService.showError(error))
      

    })
  }
  private createDataProviderMap(): Promise<any> {
    return new Promise( resolve => {

      // dataProvider
      let dataProvider = {
        "map"             : "worldLow",
        "getAreasFromMap" : true,
        "images"          : [],
      };


      // dataProvider images
      for (let i = 0 ; i < 241 ; i += 1 ) {
        
        if( ! this.userStatics[i].code ) continue;

        let dataItem: IuserStatics = this.userStatics[i];
        let id = dataItem.code.toUpperCase();
        
        if( !latLong[id] || dataItem.totalUsers == 0 ) continue;

        dataProvider.images.push({
          "id"        : id,
          "type"      : 'circle',
          "width"     : 15,
          "height"    : 15,
          "color"     : '#3a7b29',
          "longitude" : latLong[id].long ,
          "latitude"  : latLong[id].lat ,
          "title"     : 'online',
          "label"     : dataItem.totalOnline,
          "labelFontSize" : "11",
          "labelColor"    : "white",
          "selectedLabelColor" : "white",
          "labelRollOverColor" : "white",
          "zoomLevel" : 3.3,
        });

        dataProvider.images.push({
          "id"        : id,
          "type"      : 'circle',
          "width"     : 15,
          "height"    : 15,
          "color"     : 'red',
          "longitude" : latLong[id].long ,
          "latitude"  : latLong[id].lat - .6   ,
          "title"     : 'offline',
          "label"     : dataItem.totalUsers -  dataItem.totalOnline,
          "labelFontSize" : "11",
          "labelColor"    : "white",
          "selectedLabelColor" : "white",
          "labelRollOverColor" : "white",
          "zoomLevel" : 3.3,
        });

        dataProvider.images.push({
          "id"          : id,
          "imageURL"    : './assets/map/star.svg',
          "width"       : 15,
          "height"      : 15,
          "color"       : 'yellow',
          "longitude"   : latLong[id].long ,
          "latitude"    : latLong[id].lat + .6,
          "title"       : 'post 5+',
          "label"       : dataItem.posts5,
          "labelFontSize" : "11",
          "labelColor"    : "white",
          "selectedLabelColor" : "white",
          "labelRollOverColor" : "white",
          "zoomLevel"   : 3.3,
        });

      }
      
      this.dataProviderMap = dataProvider ; 

      return resolve();

    })
  }
  private makeMap(): void {
    this.map = new AmCharts.makeChart('usersAroundWorldMap' , {
      "type": "map",
      "theme" : "black",
      "addClassNames": true,
      "dataProvider" : this.dataProviderMap ,
      "mouseWheelZoomEnabled": true,
      "projection" : 'eckert3',
      "imagesSettings" : {
        // "balloonText": '<span style="font-size:14px;"><b>[[title]]</b>: [[value]]</span>',
        "alpha": 1,
        "rollOverScale": 1.8,
        "selectedScale": 1.8,
      },
      "areasSettings": {
        "autoZoom": true,
        "alpha": 1,
        "selectedColor" : "#57c304",
        "selectedOutlineColor" : "#60c307 ",
        "selectable": true
      },
      // "smallMap": {},
      "listeners": [{
        "event": "descriptionClosed",
        "method":this.listenersMap.descriptionClosed
      },{
        "event": "clickMapObject",
        "method":this.listenersMap.clickMapObject
      },{
        "event": "rendered",
        "method":this.listenersMap.rendered
      },]
    });
  }
  private get listenersMap(): any {

    const listeners: any = {};

    listeners.clickMapObject = event => {

      this.map.selectObject();
      
      if( typeof event.mapObject.user_id !== 'undefined') {
        // is user
        this.setUserDetails(event.mapObject.user_id);
      } else {
        // is country
        this.setCountryDetails(event.mapObject.id);
      }

      this.autoComplete = '';
      this.setUsersInDataProviderImages( this.map.getObjectById(event.mapObject.id) , event.mapObject.id );
      
    }

    listeners.rendered =  (event) => {
      // this.setCountryDetails( this.bigCountryOfUsers.code );
      // event.chart.selectObject( event.chart.getObjectById( this.bigCountryOfUsers.code ) );
    }

    listeners.descriptionClosed =  (event) => {
      this.closeDetails();
      event.chart.selectObject();
    }
    
    return listeners;
  }
  private async initMap() {
    await this.getUserStatics();
    await this.createDataProviderMap();
    this.makeMap();
  } 

  // title
  private setTitle( title: string): void {
    this.titleService.setTitle( title );
  }

  // set all options
  private setOptions(): void {
    this.countriesHaveUsers = [];
    this.allUsers = [];
    this.userUrl = environment.userUrl;
  };

  public ngOnInit() {
    this.setTitle('Users Around World Map');
    this.initMap()
    this.jquery();
    this.setOptions();
  }

}



