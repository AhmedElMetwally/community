import { LoadingService    } from './../../../../../service/loading.service';
import { ActivatedRoute    } from '@angular/router';
import { ErrorService      } from './../../../../../service/error.service';
import { EmployeesService  } from './../../../../../service/employee.service';
import { Component, OnInit } from '@angular/core';

import * as dayjs from 'dayjs'

@Component({
  selector: 'app-employee-charts',
  templateUrl: './employee-charts.component.html',
  styleUrls: ['./employee-charts.component.css']
})
export class EmployeeChartsComponent implements OnInit {

  constructor(
    private employeesService: EmployeesService,
    private errorService:     ErrorService,
    private route:            ActivatedRoute,
    private LoadingService:   LoadingService
  ){};

  public chartData: any;
  private _id:      string;
  
  private getParams(): Promise<any> {
    return new Promise( resolve => {
      this.route
        .params
        .subscribe( params => {
          this._id = params._id;
          resolve()
        })
    })
  }
  
  private getComputedLogs(): void {
    
    this.employeesService.getComputedLogs( this._id )
    .then( ({data}) => {

      let arr = new Array(12).fill(0)

      data.forEach( ( v ) => {
        arr[ parseInt( v.currentMonth ) ] = Math.round( v.statics) ;
      })

      
      this.chartData = {
        labels: [
          'January', 'February', 'March', 'April', 
          'May', 'June', 'July' , 'August' , 
          'September' , 'October' ,'November' , 'December'
        ],
        datasets: [
          {
            label: 'Hours',
            backgroundColor: 'green',
            borderColor: '#3c763d',
            fill: false,
            data: arr,
          },
        ]
      };

      this.LoadingService.hide();

    })
    .catch((e) => this.errorService.showError( e ) );
    
  }

  public async ngOnInit() {
    this.LoadingService.show();
    await this.getParams();
    this.getComputedLogs()

  }

}
