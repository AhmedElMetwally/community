import { environment    } from './../../../environments/environment';
import { LoadingService } from './../../service/loading.service';
import { SelectItem     } from 'primeng/components/common/selectitem';
import { ErrorService   } from './../../service/error.service';
import { UsersService   } from './../../service/user.service';
import { Title          } from '@angular/platform-browser';

import { Component, OnInit } from '@angular/core';

declare let $;

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.css']
})
export class ViewUsersComponent implements OnInit {

    constructor(
        private titleService:    Title,
        private loadingService:  LoadingService,
        private usersService:    UsersService,
        private errorService:    ErrorService
  
    ) { }
    
    public users: any[];
  
    public filterOptions: SelectItem[];
    public filterBy:      string;
    public filterModel:   any;
  
    public searchModel:   string;
    public searchMode:    boolean;
  
    public userUrl:  string;
  
    private page:    number;
    private limit:   number;
  
    public loading: boolean;
  
    public usersLength: number;
  
    public lockScroll: boolean;
    
    public scrollToTop(): void {
      $("html, body").animate({ scrollTop: 0 }, 600);
    }
  
    private setUsers( users: any[] ): void {
      let len = users.length;
      this.lockScroll = len !== this.limit
      this.loading = false;
      this.users = [...this.users , ...users];
      this.usersLength += len;
    } 
  
    public onScroll(): void {
  
      if( this.lockScroll ) return;
  
      this.loading = true;
  
      if ( this.searchMode ) {
        this.usersService.getUsersSearch( this.page , this.limit  , this.searchModel )
        .then(({ data }) => this.setUsers( [data] ) )
        .catch((e) => this.errorService.showError( e ) );
      } else {
        this.usersService.getUsers('global' , this.page , this.limit  , this.filterBy )
        .then(({ data }) => this.setUsers( data ) )
        .catch((e) => this.errorService.showError( e ) );
      }
  
      this.page += 1;
      this.lockScroll = true;
      
    }
  
    private reset(): void {
      this.page = 1;
      this.limit = 16;
      
      this.users = [];
      this.usersLength = 0;
      this.lockScroll = false;
      
      this.onScroll()
    }
  
    public filterOnChange( { value } ): void {  
      this.filterBy = value;
      this.reset();
    }
  
    private setTitle( title ): void {
      this.titleService.setTitle( title );
    }
  
    public Search(): void {
        if(typeof this.searchModel !== 'undefined' ) {
            if( this.searchModel !== '' ){
                this.searchMode = true;
                this.reset();
            }
        }
    }
    public closeSearch(): void {
        if( this.searchMode === true ) {
            this.searchModel = '';
            this.searchMode = false;
            this.reset();
        }
    }
    public searchKeyUp( event: any): void {
      if (event.keyCode === 13 ) {
        this.Search()
      }
    }
  
    private setOptions(): Promise<any> {
      return new Promise( resolve => {
  
        this.loading = true;
  
        this.userUrl = environment.userUrl;
  
        this.filterOptions = [
            {label: 'all users', value: 'all'},
            {label: 'online', value: 'online'},
            {label: 'offline', value: 'offline'},
            {label: 'posts 5+', value: 'active'},
        ];
        this.filterModel = 'all users';
        this.filterBy = 'all';
  
        this.searchMode = false;
        
        resolve();
  
      })
    }
    
    private jquery(): void {
      setTimeout(() => {
        
        const scrollFunc = () => {
          if( $(window).scrollTop() === 0 ) {
            $('.scroll-top-btn').removeClass('scroll-top-btn-show').addClass('scroll-top-btn-hide');
          } else {
            $('.scroll-top-btn').removeClass('scroll-top-btn-hide').addClass('scroll-top-btn-show');
          }
        }
  
        $(window).scroll( scrollFunc )
  
  
      } , 250)
    }
  
    public async ngOnInit() {
      this.setTitle('View Users');
      this.jquery();
      await this.setOptions();
      this.reset();
    }
  
  
  
}
  
  
  
  
   


 