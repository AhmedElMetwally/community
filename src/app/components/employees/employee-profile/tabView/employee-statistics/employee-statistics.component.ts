import { LoadingService    } from './../../../../../service/loading.service';
import { ErrorService      } from './../../../../../service/error.service';
import { ActivatedRoute    } from '@angular/router';
import { Iemployee         } from './../../../../../domain/Iemployee';
import { EmployeesService  } from './../../../../../service/employee.service';
import { Component, OnInit } from '@angular/core';

import * as dayjs from 'dayjs'

declare let $;

type Ilog = {
  currentMonth: number;
  currentYear:  number;
  _id:          string;
  logTo:        string;
  logFrom:      string;
};

@Component({
  selector: 'app-employee-statistics',
  templateUrl: './employee-statistics.component.html',
  styleUrls: ['./employee-statistics.component.css']
})
export class EmployeeStatisticsComponent implements OnInit {

  constructor(
    private employeesService: EmployeesService,
    private route :           ActivatedRoute,
    private errorService:     ErrorService,
    private LoadingService:   LoadingService
  ) { };

  private _id: string;

  public logs: Ilog[];

  private page:   number;
  private limit:   number;

  public loading: boolean;

  public lockScroll: boolean;
  
  public monthes: string[];

  
  public scrollToTop(): void {
    $("html, body").animate({ scrollTop: $('.__view_employee').height() }, 600);
  }

  private jquery(): void {
    setTimeout(() => {
      
      const scrollFunc = () => {
        if( $(window).scrollTop() < $('.__view_employee').height()  ) {
          $('.scroll-top-btn').removeClass('scroll-top-btn-show').addClass('scroll-top-btn-hide');
        } else {
          $('.scroll-top-btn').removeClass('scroll-top-btn-hide').addClass('scroll-top-btn-show');
        }
      }

      $(window).scroll( scrollFunc )


    } , 250)
  }


  private setlogs( logs: Ilog[] ): void {
    let len = logs.length;
    this.lockScroll = len !== this.limit
    this.loading = false;
    this.logs = [...this.logs , ...logs];
  } 

  private msToTime( duration ) {
    let milliseconds = parseInt( (duration % 1000) / 100 + '');
    let seconds = parseInt((duration / 1000) % 60 + '');
    let minutes = parseInt((duration / (1000 * 60)) % 60 + '');
    let hours = parseInt((duration / (1000 * 60 * 60)) % 24 + '');
  
    let h = (hours < 10) ? "0" + hours : hours;
    let m = (minutes < 10) ? "0" + minutes : minutes;
    let s = (seconds < 10) ? "0" + seconds : seconds;
  
    return h + ":" + m + ":" + s ;
  }
  

  public onScroll(): void {
    if( this.lockScroll ) return;

    this.loading = true;

    this.employeesService.getLogs( this._id , this.page , this.limit  , new Date().getFullYear() )
    .then(({ data }) => {

      this.LoadingService.hide();

      this.setlogs( 


        data.map( (v: Ilog) => {

          const currentDate = new Date(v.logFrom);
          const toDate      = new Date(v.logTo);
          const diff        = Math.floor(( (toDate as any ) - ( currentDate as any) )   ) ; 

          return {
            ...v,
            date : `${v.currentYear} ${this.monthes[ v.currentMonth ]} ${new Date(v.logTo).getDate()}` ,
            from : dayjs(v.logFrom).format('YYYY-MM-DD hh:mm:ss A'),
            to   : dayjs(v.logTo).format('YYYY-MM-DD hh:mm:ss A'),
            diff : this.msToTime( diff ),
          }

        })
      );

    })
    .catch((e) => this.errorService.showError( e ) );

    this.page += 1;
    this.lockScroll = true;
    
  }

  private reset(): void {
    this.page = 1;
    this.limit = 30;
    
    this.logs = [];
    this.lockScroll = false;
    this.onScroll()
  }

  private setOptions(): Promise<any> {
    return new Promise( resolve => {
      this.loading = true;
      this.monthes =  ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
      resolve();
    })
  }

  private getParams(): Promise<any> {
    return new Promise( resolve => {
      this.route
        .params
        .subscribe( params => {
          this._id = params._id;
          resolve()
        })
    })
  }

  public async ngOnInit() {
    this.LoadingService.show();
    await this.getParams();
    await this.setOptions();
    this.jquery()
    this.reset();
  }





}
