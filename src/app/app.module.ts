import { BrowserModule    } from '@angular/platform-browser';
import { NgModule         } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { ImagesComponent   } from './components/images/images.component';
import { AppComponent      } from './app.component';
import { LoadingComponent  } from './components/loading/loading.component';
import { SigninComponent   } from './components/signin/signin.component';
import { Error404Component } from './components/error404/error404.component';

import { FormsModule, ReactiveFormsModule       } from '@angular/forms';
import { isEmployeeGuard, isNotEmployeeGuard    } from './guards/employee.guard';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

@NgModule({
  
  declarations: [
    AppComponent,
    Error404Component,
    LoadingComponent,
    ImagesComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],

  providers: [
    { 
      provide: LocationStrategy, 
      useClass: HashLocationStrategy 
    },
  ],

  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
