import { Location } from '@angular/common';
import { AuthService } from './../../../service/auth.service';
import { SelectItem       } from 'primeng/components/common/selectitem';
import { environment      } from './../../../../environments/environment';
import { UsersService     } from './../../../service/user.service';
import { ErrorService     } from './../../../service/error.service';
import { EmployeesService } from './../../../service/employee.service';
import { LoadingService   } from './../../../service/loading.service';
import { Title            } from '@angular/platform-browser';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, Event, NavigationEnd } from '@angular/router';



declare let swal , $;

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit , OnDestroy {
  
  constructor (
    private route :           ActivatedRoute,
    private router :          Router,
    private titleService:     Title,
    private loadingService:   LoadingService,
    private usersService:     UsersService,
    private errorService:     ErrorService,
    private authService :     AuthService,
    private location:        Location
  ) { };
 
  public id:       string ;
  public userToken: string ;
  public user:      any ;
  public edit:      any;

  public userUrl:     string;

  public genders: SelectItem[];

  public isSuperEmployee: boolean;

  private routerEventsSubscribe: any;


  public addImage( event , form ): void {

    const formData: FormData = new FormData();
    formData.append('photos', event.files[0]  );

    this.loadingService.show()
    
    this.usersService.uploadImage( formData , this.userToken )
    .then( res => {

      form.clear();
      
      this.usersService.updateImageId({ photoId : res.id } , this.userToken )
      .then( () => {
        this.user.profile.profileImage = res.profileImage
        this.loadingService.hide();
      })
      .catch( error => this.errorService.showError( error ) )

    })
    .catch( error => this.errorService.showError( error ) )

  }

  private setOptions(): void {


    this.genders = [
      {label: 'Male', value: 'male'},
      {label: 'Female', value: 'female'},
      {label: 'other', value: 'other' }
    ];

    this.userUrl = environment.userUrl;
    
    this.edit = {

      firstName : false,
      lastName : false,

      gender: false,
      birthday: false,

      description: false,

      email : false,
      mobileNumber: false,

      education : false,
      jobTitle : false,

      country : false,
    };

    this.isSuperEmployee = this.authService.isSuperEmployee()

  }

  private setTitle( title ): void {
    this.titleService.setTitle( title );
  }

  private getParams(): Promise<any> {
    return new Promise( resolve => {
      this.route
        .params
        .subscribe( params => {
          this.id = params.id;
          resolve()
        })
    })
  }

  public editUserOnKeyUp( key , value , event ) {
    if( event.keyCode != 13 ) return;
    this.editUser(key , value)
  }

  public editUser( key , value ) {

    this.loadingService.show();

    let obj: any = {
      profile : {
        id           : this.user.profile.id,
        createdAt    : this.user.profile.createdAt,
        updatedAt    : this.user.profile.updatedAt,
        firstName    : this.user.profile.firstName,
        lastName     : this.user.profile.lastName,
        language     : this.user.profile.language,
        location     : this.user.profile.location,
        country      : this.user.profile.country,
        countryCode  : this.user.profile.countryCode,
        countryDialCode : this.user.profile.countryDialCode,
        gender       : this.user.profile.gender,
        birthdate    : this.user.profile.birthdate,
        description  : this.user.profile.description,
        education    : this.user.profile.education,
        jobTitle     : this.user.profile.jobTitle,
        facebookLink : this.user.profile.facebookLink,

      },
      user    : {
        email        : this.user.email,
        id           : this.user.id,
        mobileNumber : this.user.mobileNumber
      } 
    };

    if(key === 'mobileNumber' || key === 'email' ) {
      obj.user[ key ] = value;
    } else {
      obj.profile[ key ] = value;
    }

    console.log(obj)

    this.usersService.editUser( obj , this.userToken )
    .then(() => {
      if(key === 'mobileNumber' || key === 'email' ) {
        this.user[ key ] = value;
      } else {
        this.user.profile[ key ] = value;
      }
      this.edit[ key ] = false;
      this.loadingService.hide();
    })
    .catch( error => this.errorService.showError(error))

  }
 
  private getUser(): Promise<any> {
    this.loadingService.show();
    return new Promise( resolve => {

      this.usersService.getUser( this.id )
      .then( ({data}) => {
        this.user      = data.result;
        this.userToken = data.token;
        
        this.setTitle(data.result.profile.firstName + ' ' + data.result.profile.lastName);
        
        this.loadingService.hide();
        resolve();
      })
      .catch( error => {

        if (error.error.message === 'User Deactivated By System'){
          this.unblockThisUser();
        } else {  
          this.errorService.showError( error )
        };

      })

    })
  }

  private async init() {
    this.setOptions();
    await this.getParams();
    await this.getUser();
  }

  public blockThisUser() {
    swal({
      title: "Are you sure?",
      text: "You Will Block This User",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {

      if (!willDelete) return swal( 'User Is Unblock' );

      this.loadingService.show();

      this.usersService.blockAndUnBlockUser( this.id , false)
      .then( res => {
        this.loadingService.hide();
        this.location.back();
        swal( 'User Is Block' );
      })
      .catch( error => this.errorService.showError(error))

    });
  }

  public unblockThisUser() {
    swal({
      title: "This User Is Blocked",
      text: "You Will Unblock This User",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {

      if (!willDelete) {
        this.location.back();
        return swal( 'User Is Block' );
      }

      this.loadingService.show();

      this.usersService.blockAndUnBlockUser( this.id , true)
      .then( res => {
        this.loadingService.hide();
        swal( 'User Is Unblock' );
        this.init();
      })
      .catch( error => this.errorService.showError(error))

    });
  }

  public ngOnInit() {
    this.init();
    this.routerEventsSubscribe = this.router.events.subscribe( (event: Event) => {
      if(event instanceof NavigationEnd && this.router.url.indexOf('view-users') !== -1) {
        this.user = null;
        this.init();
      }
    });

  };

  public ngOnDestroy() {
    this.routerEventsSubscribe.unsubscribe();
  }

}
