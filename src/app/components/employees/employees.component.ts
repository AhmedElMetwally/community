import { AuthService } from './../../service/auth.service';
import { ErrorService     } from './../../service/error.service';
import { environment      } from './../../../environments/environment';
import { EmployeesService } from './../../service/employee.service';
import { ActivatedRoute   } from '@angular/router';
import { LoadingService   } from './../../service/loading.service';

import { Title      } from '@angular/platform-browser';
import { SelectItem } from 'primeng/components/common/selectitem';
import { Iemployee  } from './../../domain/Iemployee';

import { Component, OnInit, ViewChild } from '@angular/core';

declare let swal , $;

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  constructor(
      private titleService:     Title,
      private loadingService:   LoadingService,
      private employeesService: EmployeesService,
      private errorService:     ErrorService,
      private authService:      AuthService

  ) { }
  
  public employees: Iemployee[];

  public filterOptions: SelectItem[];
  public filterBy:      string;
  public filterModel:   any;

  public searchModel:   string;
  public searchMode:    boolean;

  public url:     string;

  private page:   number;
  private limit:   number;

  public loading: boolean;

  public employeesLength: number;

  public lockScroll: boolean;
  
  public isSuperEmployee: boolean;
  

  public scrollToTop(): void {
    $("html, body").animate({ scrollTop: 0 }, 600);
  }

  private setEmployees( employees: any[] ): void {
    let len = employees.length;
    this.lockScroll = len !== this.limit
    this.loading = false;
    this.employees = [...this.employees , ...employees];
    this.employeesLength += len;
  } 

  public onScroll(): void {

    if( this.lockScroll ) return;

    this.loading = true;

    if ( this.searchMode ) {
      this.employeesService.getEmployeesSearch( this.page , this.limit  , this.searchModel )
      .then(({ data }) => this.setEmployees( data ) )
      .catch((e) => this.errorService.showError( e ) );
    } else {
      this.employeesService.getEmployees( this.page , this.limit  , this.filterBy )
      .then(({ data }) => this.setEmployees( data ) )
      .catch((e) => this.errorService.showError( e ) );
    }

    this.page += 1;
    this.lockScroll = true;
    
  }

  private reset(): void {
    this.page = 1;
    this.limit = 16;
    
    this.employees = [];
    this.employeesLength = 0;
    this.lockScroll = false;
    
    this.onScroll()
  }

  public filterOnChange( { value } ): void {  
    this.filterBy = value;
    this.reset();
  }

  private setTitle( title ): void {
    this.titleService.setTitle( title );
  }

  public Search(): void {
      if(typeof this.searchModel !== 'undefined' ) {
          if( this.searchModel !== '' ){
              this.searchMode = true;
              this.reset();
          }
      }
  }
  public closeSearch(): void {
      if( this.searchMode === true ) {
          this.searchModel = '';
          this.searchMode = false;
          this.reset();
      }
  }
  public searchKeyUp( event: any): void {
    if (event.keyCode === 13 ) {
      this.Search()
    }
  }

  private setOptions(): Promise<any> {
    return new Promise( resolve => {

      this.loading = true;

      this.url = environment.url;

      this.filterOptions = [
          {label: 'all employees', value: null},
          {label: 'public official', value:  0 },
          {label: 'advertising officials', value: 1 },
          {label: 'technical support officer', value: 2 },
      ];
      this.filterModel = 'all employees';
      this.filterBy = null;

      this.searchMode = false;

      this.isSuperEmployee = this.authService.isSuperEmployee()
      
      resolve();

    })
  }

  public getGroups( arr: string[] ) {
    enum groups {
        'public official' = 0,
        'advertising officials',
        'technical support officer',
    } 
    return arr.map( v => {
      return groups[v]
    }).join(' - ')
  }
  
  private jquery(): void {
    setTimeout(() => {
      
      const scrollFunc = () => {
        if( $(window).scrollTop() === 0 ) {
          $('.scroll-top-btn').removeClass('scroll-top-btn-show').addClass('scroll-top-btn-hide');
        } else {
          $('.scroll-top-btn').removeClass('scroll-top-btn-hide').addClass('scroll-top-btn-show');
        }
      }

      $(window).scroll( scrollFunc )


    } , 250)
  }

  public async ngOnInit() {
    this.jquery();
    this.setTitle('View Employees');
    await this.setOptions();
    this.reset();
  }




}




 