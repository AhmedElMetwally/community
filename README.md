# Community

## to fix primeng

```javascript
// open src 1
// node_modules/primeng/components/datatable/datatable.d.ts
import { Subscription } from 'rxjs/Subscription';  
// to
import { Subscription } from 'rxjs';  


// open src 2
// node_modules/primeng/components/common/messageservice.d.ts
import { Observable } from 'rxjs/Observable';
// to 
import { Observable } from 'rxjs';


// open src 3
// node_modules/primeng/components/common/messageservice.js
var Subject_1 = require("rxjs/Subject");
// to 
var Subject_1 = require("rxjs");


// open src 3
// node_modules/primeng/components/messages/messages.d.ts
import { Subscription } from 'rxjs/Subscription';
// to
import { Subscription } from 'rxjs';
```

