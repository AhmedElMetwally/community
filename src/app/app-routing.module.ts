import { isNotEmployeeGuard, isEmployeeGuard, isHaveRoleGuard } from './guards/employee.guard';
import { SigninComponent } from './components/signin/signin.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { Error404Component } from './components/error404/error404.component';

@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path : '',
                pathMatch: 'full',
                redirectTo : 'signin',
            },
            {
                path : 'signin',
                canActivate : [ isNotEmployeeGuard ],
                loadChildren: './components/signin/signin.module#SigninModule' ,
            },

            {
                path: 'home',
                canActivate : [ isEmployeeGuard ],
                loadChildren: './components/home/home.module#HomeModule' ,
            },
            {
                path: 'users-around-the-world',
                canActivate : [ isEmployeeGuard , isHaveRoleGuard],
                loadChildren: './components/users-around-world-map/users-around-world-map.module#UsersAroundWorldMapModule',
                data : {
                    groups : [
                        0
                    ]
                }
            },
            {
                path : 'view-users',
                canActivate : [ isEmployeeGuard , isHaveRoleGuard],
                loadChildren : './components/view-users/view-users.module#ViewUsersModule',
                data : {
                    groups : [
                        0
                    ]
                }
            },
            {
                path : 'my-profile',
                canActivate : [ isEmployeeGuard ],
                loadChildren : './components/my-profile/my-profile.module#MyProfileModule',
            },
            {
                path : 'employees',
                canActivate : [ isEmployeeGuard , isHaveRoleGuard ],
                loadChildren : './components/employees/employees.module#EmployeesModule',
                data : {
                    groups : [
                        0
                    ]
                }
            },
            {
                path : 'advertisements',
                canActivate : [ isEmployeeGuard , isHaveRoleGuard ],
                loadChildren : './components/advertisements/advertisements.module#AdvertisementsModule',
                data : {
                    groups : [
                        0 , 1
                    ]
                }
            },



            {
                path: '**',
                component: Error404Component ,
                // canActivate : [ isEmployeeGuard ],
                data : {title : 'Error 404'}
            },

        ])
    ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
