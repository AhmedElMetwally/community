import { AdvertisementsService } from './../../../../../../service/advertisements.service';
import { IuserAroundWorld      } from './../../../../../../domain/IuserAroundWorld';
import { ActivatedRoute        } from '@angular/router';
import { LoadingService        } from '../../../../../../service/loading.service';
import { Title                 } from '@angular/platform-browser';

import { Component, OnInit, ViewChild    } from '@angular/core';
import { CountriesCode_Name as countries } from './../../../../../users-around-world-map/countriescode-name';

declare const google: any , $: any , swal  ;


@Component({
  selector: 'app-user-advertisement-map',
  templateUrl: './advertisement-map.component.html',
  styleUrls: ['./advertisement-map.component.css']
})
export class UsersAdvertisementMapComponent implements OnInit {
  
  constructor(
    private loadingService: LoadingService,
    private advertisementsService: AdvertisementsService,
    
  ){};
  
  public mapOptions:  any;
  public mapOverlays: any[];
  public gmap:        any;

  public useGpsMood: boolean;

  public location:   number[]; // [long , lat ]

  public countries: any[];


  private setMapOptions() {
    let location = [30 , 30];
    this.mapOptions = {
      center: { 
        lat: location[1] , 
        lng: location[0]
      },
      zoom: 7
    };
    this.useGpsMood = false;
    this.location = [];
  }
 
  private getLocationByGps(): void {
    
    if( 'geolocation' in navigator ) {

      this.loadingService.show()
      
      navigator.geolocation
      .getCurrentPosition( 

        ( position: any ) => {
          
          this.useGpsMood = true;

          this.location = [position.coords.longitude ,position.coords.latitude];

          this.setMapCenter(this.location)

          this.addMarker(this.location);

          this.loadingService.hide()
        } , 
        
        ( error: any ) => {
          this.loadingService.hide();
          swal( 'GPS' , 'Can`t Get Location By GPS' , "error");
        } , {
          timeout: 5000,
        }

      );

    }

  }

  private setMapCenter( location ): void {
    this.gmap.setCenter(
      new google.maps.LatLng
      ( 
        location[1],  
        location[0]
      )
    )
    this.gmap.setZoom(5);
    this.addMarker(location);
    this.useGpsMood = true;
  }

  public handleMapClick( event ): void {
    if( ! this.useGpsMood ) {
      this.location = [ event.latLng.lng() , event.latLng.lat() ]
      this.addMarker( this.location )
    }
  }

  private addMarker(location) {
    this.mapOverlays = [];
    this.mapOverlays.push(
        new google.maps.Marker({
          position:{
            lat: location[1], 
            lng: location[0]
          }
      }));
  }

  public useMouseToGetLocation(): void {
    this.useGpsMood = false;
    this.mapOverlays = [];
    this.location = []  
  }

  private setOptions(): void {
    // this.countries = countries;
  }

  public get autoCompleteEvents(): any {
    const events: any = {};
    events.onSelect = (event): void => {
      this.advertisementsService.getLocationByCode( event.code )
      .then( ({latlng}) => {
        this.useMouseToGetLocation();
        this.location = latlng;
        this.setMapCenter(this.location);
      })
    };

    events.onClear = (): void => {
    };

    events.completeMethod = ( event ): void => {
      this.countries = [];
        for(let i = 0, len = countries.length ; i < len ; i++) {
            let CountryCode_Name = countries[i];
            if( CountryCode_Name.name.toLowerCase().indexOf(event.query.toLowerCase()) == 0 ) {
                this.countries.push(CountryCode_Name);
            }
        }
    }
    return events;
  }

  public setMap(event) {
    this.gmap = event.map;
  }

  public ngOnInit() {
    this.setMapOptions();
    this.setOptions();
  }

}
