import { ActivatedRoute } from '@angular/router';
import { UsersService   } from './../../../../../service/user.service';
import { ErrorService   } from './../../../../../service/error.service';
import { LoadingService } from './../../../../../service/loading.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  constructor(
    private loadingService: LoadingService,
    private errorService:   ErrorService,
    private route:          ActivatedRoute,
    private usersService:   UsersService,
  ) { };

  public likesData: any;
  public disLikesData: any;
  public postsData: any;
  public onlineData: any;
  
  public id:        string;
  public userToken: string;

  public chartOptions: any ;

  private setOptions(): void {
    this.chartOptions = {
      legend: {display: false}
    }
  };

  private getParams(): Promise<any> {
    return new Promise( resolve => {
      this.route
        .params
        .subscribe( params => {
          this.id = params.id;
          resolve()
        })
    })
  }

  private getUserChartData(): void  {
    this.loadingService.show();

    this.usersService.getUser( this.id )
    .then(({ data }) => {
      this.userToken = data.token;

      this.usersService.getStatics( this.id  )
      .then( ({ data }) => {

        let likes       = new Array(12).fill(0); 
        let disLikes    = new Array(12).fill(0); 
        let posts       = new Array(12).fill(0); 
        let onlineHours = new Array(12).fill(0); 
  
        data.forEach( v => {
          likes[       parseInt( v.currentMonth) ]  =  parseInt( v.totalLikes || 0 );
          disLikes[    parseInt( v.currentMonth) ]  =  parseInt( v.totalDislikes || 0 );
          posts[       parseInt( v.currentMonth) ]  =  parseInt( v.totalPosts || 0 );
          onlineHours[ parseInt( v.currentMonth) ]  =  parseInt( v.totalOnlineHours || 0 );
        });

        this.likesData = {
        
          labels: [
            'January', 'February', 'March', 'April', 
            'May', 'June', 'July' , 'August' , 
            'September' , 'October' ,'November' , 'December'
          ],
    
          datasets: [
            {
              label: 'Likes',
              backgroundColor: '#36A2EB',
              borderColor: '#1E88E5',
              data: likes
            },
          ]
    
    
        };

        this.disLikesData = {
        
          labels: [
            'January', 'February', 'March', 'April', 
            'May', 'June', 'July' , 'August' , 
            'September' , 'October' ,'November' , 'December'
          ],
    
          datasets: [
            {
              label: 'DisLikes',
              backgroundColor: 'gray',
              borderColor: '#3e3d3d',
              data: disLikes 
            },
          ]
    
    
        };

        this.postsData = {
        
          labels: [
            'January', 'February', 'March', 'April', 
            'May', 'June', 'July' , 'August' , 
            'September' , 'October' ,'November' , 'December'
          ],
    
          datasets: [
            {
              label: 'Posts',
              backgroundColor: '#f39c12',
              borderColor: '#c7851c',
              data: posts
            }
          ]
    
    
        };

        this.onlineData = {
        
          labels: [
            'January', 'February', 'March', 'April', 
            'May', 'June', 'July' , 'August' , 
            'September' , 'October' ,'November' , 'December'
          ],
    
          datasets: [
            {
              label: 'Hours',
              backgroundColor: 'green',
              borderColor: '#015001',
              data: onlineHours
            }
          ]
    
    
        };

        this.loadingService.hide();

      })
      .catch( e => this.errorService.showError( e ));

    })
    .catch( e => this.errorService.showError( e ));

  }
 
  public async ngOnInit() {
    this.setOptions();
    await this.getParams()    
    this.getUserChartData()    
  }


}
