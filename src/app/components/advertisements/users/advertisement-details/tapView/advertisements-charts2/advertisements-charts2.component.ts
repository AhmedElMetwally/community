import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-advertisements-charts2',
  templateUrl: './advertisements-charts2.component.html',
  styleUrls: ['./advertisements-charts2.component.css']
})
export class UsersAdvertisementsCharts2Component implements OnInit {


  public Data: any;

  // min & max dates 
  public minDate:any;
  public maxDate:any;

  // ranbe model
  public rangeDates: any;

  public chartOptions: any ;


  private setOptions(): void {
    this.chartOptions = {
      legend: {display: false}
    }
  };

  constructor(

  ) { 

    this.Data = {
      
      labels: [
        'from 8 to 18', 'from 18 to 25', 'from 25 to 50', 'from 50', 
      ],
  
      datasets: [
          {
              label: 'Users',
              borderColor: '#6ace1e',
              backgroundColor: '#6edd19',
              fill: false,
              data: [
                50,
                125, 
                49, 
                80, 
              ]
          },
      ]
    };

  }


  ngOnInit() {
    this.minDate = new Date('2018/05/02')
    this.maxDate = new Date('2018/05/22')

    this.setOptions()
  }

}
