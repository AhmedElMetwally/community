import { AuthService      } from './../../../service/auth.service';
import { ErrorService     } from './../../../service/error.service';
import { environment      } from './../../../../environments/environment';
import { Iemployee        } from './../../../domain/Iemployee';
import { EmployeesService } from './../../../service/employee.service';
import { LoadingService   } from './../../../service/loading.service';
import { Title            } from '@angular/platform-browser';

import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit      } from '@angular/core';

declare let swal;

@Component({
  selector: 'app-employee-profile',
  templateUrl: './employee-profile.component.html',
  styleUrls: ['./employee-profile.component.css']
})
export class EmployeeProfileComponent implements OnInit {

  constructor (
    private route :           ActivatedRoute,
    private router :          Router,
    private titleService:     Title,
    private loadingService:   LoadingService,
    private employeesService: EmployeesService,
    private errorService:     ErrorService,
    private authService:      AuthService
  ) { };
 
  public _id:      string ;
  public employee: Iemployee ;
  public edit:     any;

  public genders: any;
  public roles:   any;
  public groups:  any;

  public url:     string;

  public isSuperEmployee: boolean;

  
  private setOptions(): void {

    this.url = environment.url;
    
    this.edit = {
      firstName : false,
      lastName : false,
      mobileNumber: false,
      email : false,
      password : false,
      salary: false,
      groups: false,
      roles: false,
    }

    this.groups = [
      {label: 'public official', value: 0 },
      {label: 'advertising officials', value: 1},
      {label: 'technical support officer', value: 2}
    ];

    this.genders = [
      {label: 'Male', value: 0},
      {label: 'Female', value: 1},
      {label: 'other', value: 2 }
    ];

    this.roles = [
        {label:'Normal Employee', value: 'NORMAL_EMPL' },
        {label:'Super Employee', value:'SUPER_EMPL' },
    ];

    this.isSuperEmployee = this.authService.isSuperEmployee()

  }

  private setTitle( t): void {
    this.titleService.setTitle( t );
  }

  public addImage( event , form ): void {

    const formData: FormData = new FormData();
    formData.append('photo', event.files[0]  );
    formData.append('id', this._id );

    this.loadingService.show()
    
    this.employeesService.addImage( formData )
    .then( ({ data }) => {
      this.employee = data;
      this.loadingService.hide();
      form.clear();
    })
    .catch( error => this.errorService.showError( error ) )

  }

  private getParams(): Promise<any> {
    return new Promise( resolve => {
      this.route
        .params
        .subscribe( params => {
          this._id = params._id;
          resolve()
        })
    })
  }

  public editEmployeeOnKeyUp( key , value , event ) {
    if( event.keyCode != 13 ) return;
    this.editEmployee(key , value)
  }

  public editEmployee( key , value ) {

    this.loadingService.show();

    if(key === 'roles') value = value.indexOf('SUPER_EMPL') >= 0 ? ['SUPER_EMPL' , 'NORMAL_EMPL'] : ['NORMAL_EMPL'];
    if(key === 'salary') value = parseFloat( value );

    let obj: any = {
      ...this.employee,
    };

    obj[ key ] = value;
    obj.id = this.employee._id;

    delete obj.createdAt;
    delete obj.updatedAt;
    delete obj._id;

    this.employeesService.editEmployee( obj )
    .then(() => {
      this.employee[ key ] = value;
      this.loadingService.hide();
      this.edit[ key ] = false;
    })
    .catch( error => this.errorService.showError(error))

  }

  public editEmployeePasswordOnKeyUp( password, event ) {
    if( event.keyCode != 13 ) return;
    this.editEmployeePassword(password )
  }

  public editEmployeePassword( password ) {

    this.loadingService.show();

    let obj: any = {};

    obj['password'] = password;
    obj.id = this.employee._id;

    this.employeesService.editEmployeePassword( obj )
    .then(() => {
      this.edit['password'] = false;
      this.loadingService.hide();
    })
    .catch( error => this.errorService.showError(error))

  }

  private getEmployee(): Promise<any> {

    this.loadingService.show();
    return new Promise( resolve => {

      this.employeesService.getEmployee( this._id )
      .then( ({data}) => {
        this.employee = data;

        this.setTitle(this.employee.firstName + ' ' + this.employee.lastName );
    
        this.loadingService.hide();
        resolve();
      })
      .catch( error => this.errorService.showError( error ) )

    })

  }

  public getGroups( arr: string[] ) {
    enum groups {
        'public official' = 0,
        'advertising officials',
        'technical support officer',
    } 
    return arr.map( v => {
        return groups[v]
    }).join(' - ')
  }

  public getRoles( arr: string[] ) {
    enum roles {
      NORMAL_EMPL = 'Normal Employee',
      SUPER_EMPL = 'Super Employee' 
    } 
    return arr.map( v => {
        return roles[v]
    }).slice( 0 , 2 ).join(' - ')
  }

  public getGender( n: number): string {
    enum EnumGender {
      Male = 0,
      Female,
      other
    } 
    return EnumGender[n]
  }

  public deleteEmployee(): void {
    swal({
      title: "Are you sure?",
      text: "You Will Delete This Employee",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (! willDelete) return  swal("Employee Is Safe!");

        this.loadingService.show();

        this.employeesService.deleteEmployee( this._id )
        .then(() => {
          swal("Employee Deleted", {
            icon: "success",
          });
          this.loadingService.hide();
          this.router.navigate(['employees']);
        })
        .catch( error => this.errorService.showError( error ) )

    });
  }

  public async ngOnInit() {
    this.setOptions();
    await this.getParams();
    await this.getEmployee();

    
  }

}
