import { FormGroup, FormControl, Validators } from '@angular/forms';

import { EmployeesService  } from './../../../service/employee.service';
import { Component, OnInit } from '@angular/core';
import { LoadingService    } from './../../../service/loading.service';
import { ErrorService      } from './../../../service/error.service';

import { Router         } from '@angular/router';
import { SelectItem     } from 'primeng/components/common/selectitem';
import { Iemployee      } from './../../../domain/Iemployee';
import { Title          } from '@angular/platform-browser';

declare let $ , swal;

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  constructor(
    private titleService:     Title, 
    private employeesService: EmployeesService,
    private loadingService:   LoadingService,
    private router:           Router,
    private errorService:     ErrorService
  ) { };

  public addEmployeeForm: FormGroup;

  public roles:    SelectItem[];
  public groups:   SelectItem[];
  public genders:  any[];

  private toTitleCase( str: string): string {
    let split =  str.replace( /[A-Z]/g , function( s: string ): any {
      return ' ' + s.toLowerCase() 
    });
    return split.replace(/\w\S*/g, function(txt: string){
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }

  private initForm(): void {

    const checkLengthPassword = (c: FormControl) => /.{6,30}/g.test( c.value ) ? null : {
      invalid: true
    };
    const checkPatternStr     = (c: FormControl) => /^[\w-\s]+$/g.test( c.value ) ? null : {
      invalid: true
    };
    const checkPatternEmail   = (c: FormControl) => /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test( c.value ) ? null : {
      invalid: true
    };
    const checkPatternPhone   = (c: FormControl) => /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test( c.value ) ? null : {
      invalid: true
    }; 
    const checkPatternNumber  = (c: FormControl) => /^\d+$/.test( c.value ) ? null : {
      invalid: true
    };

    this.addEmployeeForm =  new FormGroup({ 
      firstName : new FormControl('' , [ Validators.required  , checkPatternStr]),
      lastName  : new FormControl('' , [ Validators.required , checkPatternStr]),
      email     : new FormControl('' , [ Validators.required , checkPatternEmail ]),
      mobileNumber : new FormControl('' , [ Validators.required , checkPatternPhone]),
      
      gender    : new FormControl('' , [ Validators.required ]),
      groups    : new FormControl('' , [ Validators.required ]),
      roles     : new FormControl('' , [ Validators.required ]),
      salary    : new FormControl('' , [ Validators.required , checkPatternNumber]),

      password : new FormControl('', [ Validators.required , checkLengthPassword ]),
    });
  }

  public onSubmit(): void {

    if( this.addEmployeeForm.invalid) {
      for(let control in this.addEmployeeForm.controls ) {

        if( this.addEmployeeForm.controls[control].invalid ) {

          let _control = this.toTitleCase( control );
          if( this.addEmployeeForm.controls[control].errors.required ) {
            swal( _control , `${_control} Is Required`  , "error");
          } else if ( this.addEmployeeForm.controls[control].errors.invalid ) {
            swal( _control , `${_control} Is Invalid`  , "error");
          } 
          break;
          
        }

      }
      return
    }


    let employee: Iemployee = {
      firstName : this.addEmployeeForm.controls.firstName.value,
      lastName  : this.addEmployeeForm.controls.lastName.value,
      gender    : Number (this.addEmployeeForm.controls.gender.value),
      email     : this.addEmployeeForm.controls.email.value,
      mobileNumber : this.addEmployeeForm.controls.mobileNumber.value,
      password  : this.addEmployeeForm.controls.password.value,
      salary    : Number( this.addEmployeeForm.controls.salary.value ),
      groups    : this.addEmployeeForm.controls.groups.value,
      roles     : this.addEmployeeForm.controls.roles.value.indexOf('SUPER_EMPL') >= 0 ? ['SUPER_EMPL' , 'NORMAL_EMPL'] : ['NORMAL_EMPL'],
    }

    this.loadingService.show()
    this.employeesService.addEmployee( employee)
    .then( res => {
      this.loadingService.hide();
      this.router.navigate(['/employees/view' , res.data.employee._id ])
      swal( "Created Employee" , '' , "success");
    })
    .catch( error => this.errorService.showError(error))

  }

  private setTitle( t ): void {
    this.titleService.setTitle( t );
  }

  private setOptions(): void {

    this.groups = [
      {
        label: 'public official', 
        value: 0 
      },
      {
        label: 'advertising officials', 
        value: 1
      },
      {
        label: 'technical support officer', 
        value: 2
      }
    ];

    this.genders = [
      {label: 'Male', value: '0'},
      {label: 'Female', value: '1'},
      {label: 'other', value: '2' }
    ];
    
    this.roles = [
      {label:'Normal Employee', value: 'NORMAL_EMPL' },
      {label:'Super Employee', value:'SUPER_EMPL' },
    ];

  }

  public getErrorMsg( obj ): string {
    if(obj){
      if( obj.required) {
        return 'required'
      } else if ( obj.invalid ){
        return 'invalid'
      }
    }
  }

  public ngOnInit() {
    this.setTitle('Add Employees');
    this.initForm();
    this.setOptions();
    this.initForm();
  }

}
