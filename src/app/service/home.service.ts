import { environment } from './../../environments/environment';
import { Injectable  } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';



@Injectable()
export class HomeService {

  constructor(
    private http: HttpClient
  ) { }

  private _headers:HttpHeaders = new HttpHeaders();
  private headers = this._headers
      .append('Content-Type', 'application/json')
      .append('Authorization', 'Bearer ' + localStorage.getItem('token'))
  ;

  private url: string = environment.url;

  public getData():  Promise<any>  {
    return this.http.get(
      `${environment.userUrl}/api/v1/analytics/home/global/statics`,
      {
        headers : this.headers
      }
    ).toPromise();
  }



}
