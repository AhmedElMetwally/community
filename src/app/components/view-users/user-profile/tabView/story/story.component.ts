import { ImagesService  } from './../../../../../service/images.service';
import { environment    } from './../../../../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import { ErrorService   } from './../../../../../service/error.service';
import { UsersService   } from './../../../../../service/user.service';
import { LoadingService } from './../../../../../service/loading.service';

import { Component, OnInit } from '@angular/core';

declare let $,swal;

@Component({
  selector: 'app-user-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.css']
})
export class StoryComponent implements OnInit {

  constructor(
    private loadingService: LoadingService,
    private imagesService:  ImagesService,
    private usersService:   UsersService,
    private errorService:   ErrorService,
    private route:          ActivatedRoute,
  ) { };
  
  public userUrl:  string;
  private id: string;
  private userToken: string;

  public stories: any[];

  public isShowLikesOrShares: boolean ;

  public showLikesOrSharesData: any[];

  public limit: number = 10;


  public loadMore: {
    likes: any;
    shares: any;
  } = {
    likes:  {},
    shares: {}
    
  };



  public likeOrShare_id_tmp : string ;
  public likeOrShare_tmp :    'likes' | 'shares' ;

  private jquery(): void {
    setTimeout(() => {
      
      const scrollFunc = () => {
        if( $(window).scrollTop() < $('.__view_user').height() ) {
          $('.scroll-top-btn').removeClass('scroll-top-btn-show').addClass('scroll-top-btn-hide');
        } else {
          $('.scroll-top-btn').removeClass('scroll-top-btn-hide').addClass('scroll-top-btn-show');
        }
      }

      $(window).scroll( scrollFunc )


    } , 250)
  }

  public scrollToTop(): void {
    $("html, body").animate({ scrollTop: $('.__view_user').height() }, 600);
  }

  private getParams(): Promise<any> {
    return new Promise( resolve => {
      this.route
        .params
        .subscribe( params => {
          this.id = params.id;
          resolve()
        })
    })
  }
  private getUser(): Promise<any>  {
    return new Promise( resolve => {
  
      this.loadingService.show();
  
      this.usersService.getUser( this.id )
      .then(({ data }) => {
        this.userToken = data.token;
        this.loadingService.hide();
        resolve();
      })
      .catch( e => this.errorService.showError( e ));
  
    })
  
  }
  
  private getStory() {
    this.loadingService.show();
    this.usersService.getStatuses(1 , 1  , false  , 'story' , this.id , this.userToken)
    .then(({ data }) => {
      this.stories = data;
      this.loadingService.hide();
    })
    .catch((e) => this.errorService.showError( e ) );
  }

  private setOptions() {

    this.userUrl = environment.userUrl;

    this.isShowLikesOrShares = false;
    this.showLikesOrSharesData = [];
  }
  
  public showImages( media ): void{

    let arr: string[] = media.map( v => {
      return this.userUrl+'/'+ v.url
    }).filter( v => {
      return v.type !== 'photo'
    });

    this.imagesService.emit({
      imagesData : arr ,
      displayImages : true
    });

  }


  public loadMoreShares(): void {

    let id = this.likeOrShare_id_tmp;
    let page; 
    
    if(typeof this.loadMore.shares[ id ] === 'undefined' ) {
    
      page  = 1;
      this.loadMore.shares[ id ] = {};
      this.loadMore.shares[ id ].page = 1;
      this.loadMore.shares[ id ].lockScroll = false;
    
    } else {
    
      if( this.loadMore.shares[ id ].lockScroll === true ) return;
      
      page  = this.loadMore.shares[ id ].page;
    
    }

    this.isShowLikesOrShares = false;
    this.loadingService.show();

    this.usersService.getShares( page , this.limit , id , this.userToken )
    .then( ({ data }) => {

      this.loadMore.shares[ id ].page += 1 ; 

      let _data: any[] = data.map( v => {
        return {
          id    : v.user.id,
          name  : `${v.user.profile.firstName} ${v.user.profile.lastName}` ,
          img   : v.user.profile.profileImage
        }
      });

      this.showLikesOrSharesData = [ ...this.showLikesOrSharesData , ..._data ]; 
  


      this.isShowLikesOrShares = true
      this.loadingService.hide();

      if(data.length !== this.limit ) this.loadMore.shares[ id ].lockScroll = true;

    });


  }
  public showShares( id ): void {
    
    this.likeOrShare_id_tmp = id;
    this.likeOrShare_tmp = 'shares';
    let page; 
    
    if(typeof this.loadMore.shares[ id ] === 'undefined' ) {
    
      page  = 1;
      this.loadMore.shares[ id ] = {};
      this.loadMore.shares[ id ].page = 1;
      this.loadMore.shares[ id ].lockScroll = false;
    
    } else {
      page  = this.loadMore.shares[ id ].page;
    }


    this.isShowLikesOrShares = false;
    this.loadingService.show();

    this.usersService.getShares( page , this.limit , id , this.userToken )
    .then( ({ data }) => {

      let _data: any[] = data.map( v => {
        return {
          id    : v.user.id,
          name  : `${v.user.profile.firstName} ${v.user.profile.lastName}` ,
          img   : v.user.profile.profileImage
        }
      });

      this.showLikesOrSharesData = _data; 

      this.loadMore.shares[ id ].page += 1 ; 

      this.isShowLikesOrShares = true
      this.loadingService.hide();

      if(data.length !== this.limit ) this.loadMore.shares[ id ].lockScroll = true;

    })


  }


  public loadMoreLikes(): void {

    let id = this.likeOrShare_id_tmp;
    let page; 
    
    if(typeof this.loadMore.likes[ id ] === 'undefined' ) {
    
      page  = 1;
      this.loadMore.likes[ id ] = {};
      this.loadMore.likes[ id ].page = 1;
      this.loadMore.likes[ id ].lockScroll = false;
    
    } else {
    
      if( this.loadMore.likes[ id ].lockScroll === true ) return;
      
      page  = this.loadMore.likes[ id ].page;
    
    }

    this.isShowLikesOrShares = false;
    this.loadingService.show();

    this.usersService.getLikes( page , this.limit , id , this.userToken )
    .then( ({ data }) => {

      this.loadMore.likes[ id ].page += 1 ; 

      let _data: any[] = data.map( v => {
        return {
          id    : v.user.id,
          name  : `${v.user.profile.firstName} ${v.user.profile.lastName}` ,
          img   : v.user.profile.profileImage
        }
      });

      this.showLikesOrSharesData = [...this.showLikesOrSharesData , ..._data]; 
  


      this.isShowLikesOrShares = true
      this.loadingService.hide();

      if(data.length !== this.limit ) this.loadMore.likes[ id ].lockScroll = true;
      
    });


  }
  public showLikes( id ): void {
    
    this.likeOrShare_id_tmp = id;
    this.likeOrShare_tmp = 'likes';
    let page; 
    
    if(typeof this.loadMore.likes[ id ] === 'undefined' ) {
    
      page  = 1;
      this.loadMore.likes[ id ] = {};
      this.loadMore.likes[ id ].page = 1;
      this.loadMore.likes[ id ].lockScroll = false;
    
    } else {
      page  = this.loadMore.likes[ id ].page;
    }


    this.isShowLikesOrShares = false;
    this.loadingService.show();


    this.usersService.getLikes( page , 10 , id , this.userToken )
    .then( ({ data }) => {

      let _data: any[] = data.map( v => {
        return {
          id    : v.user.id,
          name  : `${v.user.profile.firstName} ${v.user.profile.lastName}` ,
          img   : v.user.profile.profileImage
        }
      });


      this.showLikesOrSharesData = _data; 

      this.loadMore.likes[ id ].page += 1 ; 

      this.isShowLikesOrShares = true
      this.loadingService.hide();

      if(data.length !== this.limit ) this.loadMore.likes[ id ].lockScroll = true;

    })




  }


  public loadMoreReplaies( id ): void {

    this.stories.forEach( v => {
      if(v.id === id ){

        if( v.lockScroll ) return ; 

        this.loadingService.show();
        
        this.usersService.getReplies( v.page , this.limit , id , this.userToken )
        .then( ({ data }) => {
    
          this.stories.forEach( v => {
            if(v.id === id ){
              
              v.showReplies = true;
              v.replaies = [...v.replaies , ...data ];
    
              v.page += 1;
    
              if(data.length !== this.limit ) v.lockScroll = true;
    
            }
          })
    
          this.loadingService.hide();
    
        })

      }
    });

    

    
  }
  public getReplies( id ): void {
    
    this.loadingService.show();

    this.usersService.getReplies( 1 , this.limit , id , this.userToken )
    .then( ({ data }) => {

      this.stories.forEach( v => {
        if(v.id === id ){


          v.showReplies = true;
          v.replaies = data;

          v.page = 2;

          if(data.length !== this.limit ) v.lockScroll = true;

        }
      })

      this.loadingService.hide();

    })





  }


  public deleteStory( id ): void {

    swal({
      title: "Are you sure?",
      text: "You Will Delete This Story",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
  
      if (! willDelete) return  swal("Story Is Safe!");
  
      this.loadingService.show();
    
      this.usersService.deleteStatuse( id  , this.userToken )
      .then(( res ) => {
  
        this.stories = this.stories.filter( v => {
          return v.id !== id 
        });          
  
        swal("Story Deleted", { icon: "success" });
  
        this.loadingService.hide();
  
      })
      .catch( error => this.errorService.showError( error ) )
  
    });
    
  }

  public deleteReplay( storyId ,  replayId , userId): void {

    swal({
      title: "Are you sure?",
      text: "You Will Delete This Replay",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then( async (willDelete) => {
  
      if (! willDelete ) return  swal("Replay Is Safe!");
  
      this.loadingService.show();
    
      let user = await this.usersService.getUser( userId ).catch(()=>{});
      let token = user.data.token;

      this.usersService.deleteStatuse( replayId  , token )
      .then(( res ) => {
  
        this.stories.forEach( v => {

          if(v.id === storyId ){

            v.counters.commentCount -=1;

            v.replaies = v.replaies.filter( v => {
              return v.id !== replayId 
            })
          }

        })
         
  
        swal("Replay Deleted", { icon: "success" });
  
        this.loadingService.hide();
  
      })
      .catch( error => this.errorService.showError( error ) )
  
    });
    
  }


  public loadMoreLikesOrShares(){
    if( this.likeOrShare_tmp == 'shares' ) {
      this.loadMoreShares()
    } else if( this.likeOrShare_tmp == 'likes' ) {
      this.loadMoreLikes()
    }
  }

  public closeShowLikesOrShares() {
    
    if( this.likeOrShare_tmp == 'shares' ) {
      this.loadMore.shares[ this.likeOrShare_id_tmp ].page = 1;
      this.loadMore.shares[ this.likeOrShare_id_tmp ].lockScroll = false;
    } else if( this.likeOrShare_tmp == 'likes' ) {
      this.loadMore.likes[ this.likeOrShare_id_tmp ].page = 1;
      this.loadMore.likes[ this.likeOrShare_id_tmp ].lockScroll = false;
    }


    this.isShowLikesOrShares = false;
  }

  public async ngOnInit() {
    this.setOptions();
    this.jquery();
    await this.getParams();
    this.getUser()
    .then( () => {
      this.getStory();
    });
  }

} 
