export const latLong = {};
latLong['AD'] = {
'lat': 42.5,
'long': 1.5
};
latLong['AE'] = {
'lat': 24,
'long': 54
};
latLong['AF'] = {
'lat': 33,
'long': 65
};
latLong['AG'] = {
'lat': 17.05,
'long': -61.8
};
latLong['AI'] = {
'lat': 18.25,
'long': -63.1667
};
latLong['AL'] = {
'lat': 41,
'long': 20
};
latLong['AM'] = {
'lat': 40,
'long': 45
};
latLong['AN'] = {
'lat': 12.25,
'long': -68.75
};
latLong['AO'] = {
'lat': -12.5,
'long': 18.5
};
latLong['AP'] = {
'lat': 35,
'long': 105
};
latLong['AQ'] = {
'lat': -90,
'long': 0
};
latLong['AR'] = {
'lat': -34,
'long': -64
};
latLong['AS'] = {
'lat': -14.3333,
'long': -170
};
latLong['AT'] = {
'lat': 47.3333,
'long': 13.3333
};
latLong['AU'] = {
'lat': -27,
'long': 133
};
latLong['AW'] = {
'lat': 12.5,
'long': -69.9667
};
latLong['AZ'] = {
'lat': 40.5,
'long': 47.5
};
latLong['BA'] = {
'lat': 44,
'long': 18
};
latLong['BB'] = {
'lat': 13.1667,
'long': -59.5333
};
latLong['BD'] = {
'lat': 24,
'long': 90
};
latLong['BE'] = {
'lat': 50.8333,
'long': 4
};
latLong['BF'] = {
'lat': 13,
'long': -2
};
latLong['BG'] = {
'lat': 43,
'long': 25
};
latLong['BH'] = {
'lat': 26,
'long': 50.55
};
latLong['BI'] = {
'lat': -3.5,
'long': 30
};
latLong['BJ'] = {
'lat': 9.5,
'long': 2.25
};
latLong['BM'] = {
'lat': 32.3333,
'long': -64.75
};
latLong['BN'] = {
'lat': 4.5,
'long': 114.6667
};
latLong['BO'] = {
'lat': -17,
'long': -65
};
latLong['BR'] = {
'lat': -10,
'long': -55
};
latLong['BS'] = {
'lat': 24.25,
'long': -76
};
latLong['BT'] = {
'lat': 27.5,
'long': 90.5
};
latLong['BV'] = {
'lat': -54.4333,
'long': 3.4
};
latLong['BW'] = {
'lat': -22,
'long': 24
};
latLong['BY'] = {
'lat': 53,
'long': 28
};
latLong['BZ'] = {
'lat': 17.25,
'long': -88.75
};
latLong['CA'] = {
'lat': 54,
'long': -100
};
latLong['CC'] = {
'lat': -12.5,
'long': 96.8333
};
latLong['CD'] = {
'lat': 0,
'long': 25
};
latLong['CF'] = {
'lat': 7,
'long': 21
};
latLong['CG'] = {
'lat': -1,
'long': 15
};
latLong['CH'] = {
'lat': 47,
'long': 8
};
latLong['CI'] = {
'lat': 8,
'long': -5
};
latLong['CK'] = {
'lat': -21.2333,
'long': -159.7667
};
latLong['CL'] = {
'lat': -30,
'long': -71
};
latLong['CM'] = {
'lat': 6,
'long': 12
};
latLong['CN'] = {
'lat': 35,
'long': 105
};
latLong['CO'] = {
'lat': 4,
'long': -72
};
latLong['CR'] = {
'lat': 10,
'long': -84
};
latLong['CU'] = {
'lat': 21.5,
'long': -80
};
latLong['CV'] = {
'lat': 16,
'long': -24
};
latLong['CX'] = {
'lat': -10.5,
'long': 105.6667
};
latLong['CY'] = {
'lat': 35,
'long': 33
};
latLong['CZ'] = {
'lat': 49.75,
'long': 15.5
};
latLong['DE'] = {
'lat': 51,
'long': 9
};
latLong['DJ'] = {
'lat': 11.5,
'long': 43
};
latLong['DK'] = {
'lat': 56,
'long': 10
};
latLong['DM'] = {
'lat': 15.4167,
'long': -61.3333
};
latLong['DO'] = {
'lat': 19,
'long': -70.6667
};
latLong['DZ'] = {
'lat': 28,
'long': 3
};
latLong['EC'] = {
'lat': -2,
'long': -77.5
};
latLong['EE'] = {
'lat': 59,
'long': 26
};
latLong['EG'] = {
'lat': 27,
'long': 30
};
latLong['EH'] = {
'lat': 24.5,
'long': -13
};
latLong['ER'] = {
'lat': 15,
'long': 39
};
latLong['ES'] = {
'lat': 40,
'long': -4
};
latLong['ET'] = {
'lat': 8,
'long': 38
};
latLong['EU'] = {
'lat': 47,
'long': 8
};
latLong['FI'] = {
'lat': 62,
'long': 26
};
latLong['FJ'] = {
'lat': -18,
'long': 175
};
latLong['FK'] = {
'lat': -51.75,
'long': -59
};
latLong['FM'] = {
'lat': 6.9167,
'long': 158.25
};
latLong['FO'] = {
'lat': 62,
'long': -7
};
latLong['FR'] = {
'lat': 46,
'long': 2
};
latLong['GA'] = {
'lat': -1,
'long': 11.75
};
latLong['GB'] = {
'lat': 54,
'long': -2
};
latLong['GD'] = {
'lat': 12.1167,
'long': -61.6667
};
latLong['GE'] = {
'lat': 42,
'long': 43.5
};
latLong['GF'] = {
'lat': 4,
'long': -53
};
latLong['GH'] = {
'lat': 8,
'long': -2
};
latLong['GI'] = {
'lat': 36.1833,
'long': -5.3667
};
latLong['GL'] = {
'lat': 72,
'long': -40
};
latLong['GM'] = {
'lat': 13.4667,
'long': -16.5667
};
latLong['GN'] = {
'lat': 11,
'long': -10
};
latLong['GP'] = {
'lat': 16.25,
'long': -61.5833
};
latLong['GQ'] = {
'lat': 2,
'long': 10
};
latLong['GR'] = {
'lat': 39,
'long': 22
};
latLong['GS'] = {
'lat': -54.5,
'long': -37
};
latLong['GT'] = {
'lat': 15.5,
'long': -90.25
};
latLong['GU'] = {
'lat': 13.4667,
'long': 144.7833
};
latLong['GW'] = {
'lat': 12,
'long': -15
};
latLong['GY'] = {
'lat': 5,
'long': -59
};
latLong['HK'] = {
'lat': 22.25,
'long': 114.1667
};
latLong['HM'] = {
'lat': -53.1,
'long': 72.5167
};
latLong['HN'] = {
'lat': 15,
'long': -86.5
};
latLong['HR'] = {
'lat': 45.1667,
'long': 15.5
};
latLong['HT'] = {
'lat': 19,
'long': -72.4167
};
latLong['HU'] = {
'lat': 47,
'long': 20
};
latLong['ID'] = {
'lat': -5,
'long': 120
};
latLong['IE'] = {
'lat': 53,
'long': -8
};
latLong['IL'] = {
'lat': 31.5,
'long': 34.75
};
latLong['IN'] = {
'lat': 20,
'long': 77
};
latLong['IO'] = {
'lat': -6,
'long': 71.5
};
latLong['IQ'] = {
'lat': 33,
'long': 44
};
latLong['IR'] = {
'lat': 32,
'long': 53
};
latLong['IS'] = {
'lat': 65,
'long': -18
};
latLong['IT'] = {
'lat': 42.8333,
'long': 12.8333
};
latLong['JM'] = {
'lat': 18.25,
'long': -77.5
};
latLong['JO'] = {
'lat': 31,
'long': 36
};
latLong['JP'] = {
'lat': 36,
'long': 138
};
latLong['KE'] = {
'lat': 1,
'long': 38
};
latLong['KG'] = {
'lat': 41,
'long': 75
};
latLong['KH'] = {
'lat': 13,
'long': 105
};
latLong['KI'] = {
'lat': 1.4167,
'long': 173
};
latLong['KM'] = {
'lat': -12.1667,
'long': 44.25
};
latLong['KN'] = {
'lat': 17.3333,
'long': -62.75
};
latLong['KP'] = {
'lat': 40,
'long': 127
};
latLong['KR'] = {
'lat': 37,
'long': 127.5
};
latLong['KW'] = {
'lat': 29.3375,
'long': 47.6581
};
latLong['KY'] = {
'lat': 19.5,
'long': -80.5
};
latLong['KZ'] = {
'lat': 48,
'long': 68
};
latLong['LA'] = {
'lat': 18,
'long': 105
};
latLong['LB'] = {
'lat': 33.8333,
'long': 35.8333
};
latLong['LC'] = {
'lat': 13.8833,
'long': -61.1333
};
latLong['LI'] = {
'lat': 47.1667,
'long': 9.5333
};
latLong['LK'] = {
'lat': 7,
'long': 81
};
latLong['LR'] = {
'lat': 6.5,
'long': -9.5
};
latLong['LS'] = {
'lat': -29.5,
'long': 28.5
};
latLong['LT'] = {
'lat': 55,
'long': 24
};
latLong['LU'] = {
'lat': 49.75,
'long': 6
};
latLong['LV'] = {
'lat': 57,
'long': 25
};
latLong['LY'] = {
'lat': 25,
'long': 17
};
latLong['MA'] = {
'lat': 32,
'long': -5
};
latLong['MC'] = {
'lat': 43.7333,
'long': 7.4
};
latLong['MD'] = {
'lat': 47,
'long': 29
};
latLong['ME'] = {
'lat': 42.5,
'long': 19.4
};
latLong['MG'] = {
'lat': -20,
'long': 47
};
latLong['MH'] = {
'lat': 9,
'long': 168
};
latLong['MK'] = {
'lat': 41.8333,
'long': 22
};
latLong['ML'] = {
'lat': 17,
'long': -4
};
latLong['MM'] = {
'lat': 22,
'long': 98
};
latLong['MN'] = {
'lat': 46,
'long': 105
};
latLong['MO'] = {
'lat': 22.1667,
'long': 113.55
};
latLong['MP'] = {
'lat': 15.2,
'long': 145.75
};
latLong['MQ'] = {
'lat': 14.6667,
'long': -61
};
latLong['MR'] = {
'lat': 20,
'long': -12
};
latLong['MS'] = {
'lat': 16.75,
'long': -62.2
};
latLong['MT'] = {
'lat': 35.8333,
'long': 14.5833
};
latLong['MU'] = {
'lat': -20.2833,
'long': 57.55
};
latLong['MV'] = {
'lat': 3.25,
'long': 73
};
latLong['MW'] = {
'lat': -13.5,
'long': 34
};
latLong['MX'] = {
'lat': 23,
'long': -102
};
latLong['MY'] = {
'lat': 2.5,
'long': 112.5
};
latLong['MZ'] = {
'lat': -18.25,
'long': 35
};
latLong['NA'] = {
'lat': -22,
'long': 17
};
latLong['NC'] = {
'lat': -21.5,
'long': 165.5
};
latLong['NE'] = {
'lat': 16,
'long': 8
};
latLong['NF'] = {
'lat': -29.0333,
'long': 167.95
};
latLong['NG'] = {
'lat': 10,
'long': 8
};
latLong['NI'] = {
'lat': 13,
'long': -85
};
latLong['NL'] = {
'lat': 52.5,
'long': 5.75
};
latLong['NO'] = {
'lat': 62,
'long': 10
};
latLong['NP'] = {
'lat': 28,
'long': 84
};
latLong['NR'] = {
'lat': -0.5333,
'long': 166.9167
};
latLong['NU'] = {
'lat': -19.0333,
'long': -169.8667
};
latLong['NZ'] = {
'lat': -41,
'long': 174
};
latLong['OM'] = {
'lat': 21,
'long': 57
};
latLong['PA'] = {
'lat': 9,
'long': -80
};
latLong['PE'] = {
'lat': -10,
'long': -76
};
latLong['PF'] = {
'lat': -15,
'long': -140
};
latLong['PG'] = {
'lat': -6,
'long': 147
};
latLong['PH'] = {
'lat': 13,
'long': 122
};
latLong['PK'] = {
'lat': 30,
'long': 70
};
latLong['PL'] = {
'lat': 52,
'long': 20
};
latLong['PM'] = {
'lat': 46.8333,
'long': -56.3333
};
latLong['PR'] = {
'lat': 18.25,
'long': -66.5
};
latLong['PS'] = {
'lat': 32,
'long': 35.25
};
latLong['PT'] = {
'lat': 39.5,
'long': -8
};
latLong['PW'] = {
'lat': 7.5,
'long': 134.5
};
latLong['PY'] = {
'lat': -23,
'long': -58
};
latLong['QA'] = {
'lat': 25.5,
'long': 51.25
};
latLong['RE'] = {
'lat': -21.1,
'long': 55.6
};
latLong['RO'] = {
'lat': 46,
'long': 25
};
latLong['RS'] = {
'lat': 44,
'long': 21
};
latLong['RU'] = {
'lat': 60,
'long': 100
};
latLong['RW'] = {
'lat': -2,
'long': 30
};
latLong['SA'] = {
'lat': 25,
'long': 45
};
latLong['SB'] = {
'lat': -8,
'long': 159
};
latLong['SC'] = {
'lat': -4.5833,
'long': 55.6667
};
latLong['SD'] = {
'lat': 15,
'long': 30
};
latLong['SE'] = {
'lat': 62,
'long': 15
};
latLong['SG'] = {
'lat': 1.3667,
'long': 103.8
};
latLong['SH'] = {
'lat': -15.9333,
'long': -5.7
};
latLong['SI'] = {
'lat': 46,
'long': 15
};
latLong['SJ'] = {
'lat': 78,
'long': 20
};
latLong['SK'] = {
'lat': 48.6667,
'long': 19.5
};
latLong['SL'] = {
'lat': 8.5,
'long': -11.5
};
latLong['SM'] = {
'lat': 43.7667,
'long': 12.4167
};
latLong['SN'] = {
'lat': 14,
'long': -14
};
latLong['SO'] = {
'lat': 10,
'long': 49
};
latLong['SR'] = {
'lat': 4,
'long': -56
};
latLong['ST'] = {
'lat': 1,
'long': 7
};
latLong['SV'] = {
'lat': 13.8333,
'long': -88.9167
};
latLong['SY'] = {
'lat': 35,
'long': 38
};
latLong['SZ'] = {
'lat': -26.5,
'long': 31.5
};
latLong['TC'] = {
'lat': 21.75,
'long': -71.5833
};
latLong['TD'] = {
'lat': 15,
'long': 19
};
latLong['TF'] = {
'lat': -43,
'long': 67
};
latLong['TG'] = {
'lat': 8,
'long': 1.1667
};
latLong['TH'] = {
'lat': 15,
'long': 100
};
latLong['TJ'] = {
'lat': 39,
'long': 71
};
latLong['TK'] = {
'lat': -9,
'long': -172
};
latLong['TM'] = {
'lat': 40,
'long': 60
};
latLong['TN'] = {
'lat': 34,
'long': 9
};
latLong['TO'] = {
'lat': -20,
'long': -175
};
latLong['TR'] = {
'lat': 39,
'long': 35
};
latLong['TT'] = {
'lat': 11,
'long': -61
};
latLong['TV'] = {
'lat': -8,
'long': 178
};
latLong['TW'] = {
'lat': 23.5,
'long': 121
};
latLong['TZ'] = {
'lat': -6,
'long': 35
};
latLong['UA'] = {
'lat': 49,
'long': 32
};
latLong['UG'] = {
'lat': 1,
'long': 32
};
latLong['UM'] = {
'lat': 19.2833,
'long': 166.6
};
latLong['US'] = {
'lat': 38,
'long': -97
};
latLong['UY'] = {
'lat': -33,
'long': -56
};
latLong['UZ'] = {
'lat': 41,
'long': 64
};
latLong['VA'] = {
'lat': 41.9,
'long': 12.45
};
latLong['VC'] = {
'lat': 13.25,
'long': -61.2
};
latLong['VE'] = {
'lat': 8,
'long': -66
};
latLong['VG'] = {
'lat': 18.5,
'long': -64.5
};
latLong['VI'] = {
'lat': 18.3333,
'long': -64.8333
};
latLong['VN'] = {
'lat': 16,
'long': 106
};
latLong['VU'] = {
'lat': -16,
'long': 167
};
latLong['WF'] = {
'lat': -13.3,
'long': -176.2
};
latLong['WS'] = {
'lat': -13.5833,
'long': -172.3333
};
latLong['YE'] = {
'lat': 15,
'long': 48
};
latLong['YT'] = {
'lat': -12.8333,
'long': 45.1667
};
latLong['ZA'] = {
'lat': -29,
'long': 24
};
latLong['ZM'] = {
'lat': -15,
'long': 30
};
latLong['ZW'] = {
'lat': -20,
'long': 30
};
