import { environment } from './../../../../environments/environment';
import { SelectItem  } from 'primeng/components/common/selectitem';
import { Title       } from '@angular/platform-browser';

import { Component, OnInit } from '@angular/core';
import { LoadingService    } from '../../../service/loading.service';


declare let $;


@Component({
  selector: 'app-users-advertisements',
  templateUrl: './advertisements.component.html',
  styleUrls: ['./advertisements.component.css']
})
export class UsersAdvertisementsComponent implements OnInit {

  constructor(
    private loadingService: LoadingService,
    private titleService:   Title,
  ) { }

  public advertisements: any[];

  // search
  public searchModel:   string;
  public searchMode:    boolean;

  public url:     string;

  private page:    number;
  private limit:   number;

  public loading: boolean;

  public advertisementsLength: number;

  public lockScroll: boolean;
  
  public scrollToTop(): void {
    $("html, body").animate({ scrollTop: 0 }, 600);
  }

  private setAdvertisements( advertisements: any[] ): void {
    let len = advertisements.length;

    this.lockScroll = len !== this.limit
    this.loading = false;
    this.advertisements = [...this.advertisements , ...advertisements];
    this.advertisementsLength += len;
  } 

  public onScroll(): void {

    if( this.lockScroll ) return;

    this.loading = true;


    setTimeout(() => {

    this.setAdvertisements( [
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      },
      {
        url : 'http://via.placeholder.com/250x200',
        date : new Date().toISOString()
      }
    ])

    } , 1000)

    // if ( this.searchMode ) {
    //   this.clientService.getClientsSearch( this.page , this.limit  )
    //   .then(({ clients }) => this.setAdvertisements( clients ) )
    // } else {
    //   this.clientService.getClients( this.page , this.limit   )
    //   .then(({ clients }) => this.setAdvertisements( clients ) )
    // }

    this.page += 1;
    this.lockScroll = true;
    
  }

  private reset(): void {
    this.page = 1;
    this.limit = 16;
    
    this.advertisements = [];
    this.advertisementsLength = 0;
    this.lockScroll = false;
    
    this.onScroll()
  }


  private setTitle( title ): void {
    this.titleService.setTitle( title );
  }

  public Search(): void {
      if(typeof this.searchModel !== 'undefined' ) {
          if( this.searchModel !== '' ){
              this.searchMode = true;
              this.reset();
          }
      }
  }
  public closeSearch(): void {
      if( this.searchMode === true ) {
          this.searchModel = '';
          this.searchMode = false;
          this.reset();
      }
  }
  public searchKeyUp( event: any): void {
    if (event.keyCode === 13 ) {
      this.Search()
    }
  }

  private setOptions(): Promise<any> {
    return new Promise( resolve => {

      this.loading = true;

      this.url = environment.url;

      this.searchMode = false;

      resolve();

    })
  }


  private jquery(): void {
    setTimeout(() => {
      
      const scrollFunc = () => {
        if( $(window).scrollTop() === 0 ) {
          $('.scroll-top-btn').removeClass('scroll-top-btn-show').addClass('scroll-top-btn-hide');
        } else {
          $('.scroll-top-btn').removeClass('scroll-top-btn-hide').addClass('scroll-top-btn-show');
        }
      }

      $(window).scroll( scrollFunc )


    } , 250)
  }

  public async ngOnInit() {
    this.jquery();
    setTimeout(() => {
      this.setTitle('View Users Advertisements');
    } , 1000 )
    await this.setOptions();
    this.reset();
  }




}
