import { AuthService } from './../../service/auth.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SigninComponent } from './signin.component';
import { SigninRoutingModule } from './signin-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SigninRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    SigninComponent
  ],
  providers : [
  ]
})
export class SigninModule { }
