import { EmployeesService            } from './../../service/employee.service';
import { AddEmployeeComponent        } from './add-employee/add-employee.component';
import { EmployeeProfileComponent    } from './employee-profile/employee-profile.component';
import { EmployeeStatisticsComponent } from './employee-profile/tabView/employee-statistics/employee-statistics.component';
import { EmployeesRoutingModule      } from './employees-routing.module';
import { EmployeeChartsComponent     } from './employee-profile/tabView/employee-charts/employee-charts.component';
import { EmployeesComponent          } from './employees.component';

import { NgModule     } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InputMaskModule  } from 'primeng/inputmask';
import { PanelModule      } from 'primeng/panel';
import { DropdownModule   } from 'primeng/dropdown';
import { DataViewModule   } from 'primeng/dataview';
import { ButtonModule     } from 'primeng/button';
import { TooltipModule    } from 'primeng/tooltip';
import { InputTextModule  } from 'primeng/inputtext';
import { TabViewModule    } from 'primeng/tabview';
import { ChartModule      } from 'primeng/chart';
import { DataTableModule  } from 'primeng/datatable';
import { FileUploadModule } from 'primeng/fileupload';

import { MultiSelectModule    } from 'primeng/multiselect';
import { InfiniteScrollModule } from "ngx-infinite-scroll";

@NgModule({
  imports: [
    CommonModule,
    EmployeesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    DataViewModule,
    DropdownModule,
    PanelModule,
    ButtonModule,
    TooltipModule,
    InputTextModule,
    TabViewModule,
    MultiSelectModule,
    ChartModule,
    DataTableModule,
    FileUploadModule,
    InputMaskModule,
  ],
  declarations: [
    EmployeesComponent,
    EmployeeProfileComponent,
    EmployeeChartsComponent,
    EmployeeStatisticsComponent,
    AddEmployeeComponent,
  ],
  providers : [
    EmployeesService
  ]
})
export class EmployeesModule { }
