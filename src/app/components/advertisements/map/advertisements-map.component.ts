import { environment } from './../../../../environments/environment';
import { SelectItem  } from 'primeng/components/common/selectitem';
import { Title       } from '@angular/platform-browser';

import { Component, OnInit } from '@angular/core';
import { LoadingService    } from '../../../service/loading.service';


declare let $,AmCharts;


@Component({
  selector: 'app-advertisements-map',
  templateUrl: './advertisements-map.component.html',
  styleUrls: ['./advertisements-map.component.css']
})
export class AdvertisementsMapComponent implements OnInit {

  constructor(
    private loadingService: LoadingService,
    private titleService:   Title,
  ) { }

  public map: any;
  public dataProviderMap =  {
    "map"             : "worldLow",
    "getAreasFromMap" : true,
    "images"          : [
      {
        "id"        : 'EG',
        "imageURL"  : './assets/map/advertisements-icon.svg',
        "width"     : 15,
        "height"    : 15,
        "color"     : '#6edd19',
        "longitude" : '30' ,
        "latitude"  : '29' ,
        "title"     : 'views : 10K',
        "label"     : 'Advertisement Name',
        "labelFontSize" : "11",
        "labelColor"    : "white",
        "selectedLabelColor" : "white",
        "labelRollOverColor" : "white",
        "zoomLevel" : 3.3,
      },
      {
        "id"        : 'EG',
        "imageURL"  : './assets/map/advertisements-icon.svg',
        "width"     : 15,
        "height"    : 15,
        "color"     : '#6edd19',
        "longitude" : '31' ,
        "latitude"  : '28' ,
        "title"     : 'views : 10K',
        "label"     : 'Advertisement Name',
        "labelFontSize" : "11",
        "labelColor"    : "white",
        "selectedLabelColor" : "white",
        "labelRollOverColor" : "white",
        "zoomLevel" : 3.3,
      },
      {
        "id"        : 'US',
        "imageURL"  : './assets/map/advertisements-icon.svg',
        "width"     : 15,
        "height"    : 15,
        "color"     : '#6edd19',
        "longitude" : '-97' ,
        "latitude"  : '38' ,
        "title"     : 'views : 150 K',
        "label"     : 'Advertisement Name',
        "labelFontSize" : "11",
        "labelColor"    : "white",
        "selectedLabelColor" : "white",
        "labelRollOverColor" : "white",
        "zoomLevel" : 3.3,
      },
      {
        "id"        : 'SA',
        "imageURL"  : './assets/map/advertisements-icon.svg',
        "width"     : 15,
        "height"    : 15,
        "color"     : '#6edd19',
        "longitude" : '45' ,
        "latitude"  : '25' ,
        "title"     : 'views : 170 K',
        "label"     : 'Advertisement Name',
        "labelFontSize" : "11",
        "labelColor"    : "white",
        "selectedLabelColor" : "white",
        "labelRollOverColor" : "white",
        "zoomLevel" : 3.3,
      }
    ],
  };



  private initMap(){
    this.map = new AmCharts.makeChart('AdvertisementsMap' , {
      "type": "map",
      "theme" : "black",
      "addClassNames": true,
      "dataProvider" : this.dataProviderMap ,
      "mouseWheelZoomEnabled": true,
      "projection" : 'eckert3',
      "imagesSettings" : {
        "alpha": 1,
        "rollOverScale": 1.8,
        "selectedScale": 1.8,
      },
      "areasSettings": {
        "autoZoom": true,
        "alpha": 1,
        "selectedColor" : "#57c304",
        "selectedOutlineColor" : "#60c307 ",
        "selectable": true
      },
      // "listeners": [{
      //   "event": "descriptionClosed",
      //   "method":this.listenersMap.descriptionClosed
      // },{
      //   "event": "clickMapObject",
      //   "method":this.listenersMap.clickMapObject
      // },{
      //   "event": "rendered",
      //   "method":this.listenersMap.rendered
      // },]
    });
  }

  private setTitle( title ): void {
    this.titleService.setTitle( title );
  }

  public async ngOnInit() {
    this.setTitle('Advertisements Map');
    this.initMap();
  }



}
