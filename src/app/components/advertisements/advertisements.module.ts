import { AdvertisementsMapComponent              } from './map/advertisements-map.component';

import { EmployeesAdvertisementsComponent        } from './employees/advertisements.component';
import { AddEmployeeAdvertisementComponent       } from './employees/add-advertisement/add-advertisement.component';
import { EmployeesAdvertisementDetailsComponent  } from './employees/advertisement-details/advertisement-details.component';
import { EmployeesAdvertisementsChartsComponent  } from './employees/advertisement-details/tapView/advertisements-charts/advertisements-charts.component';
import { EmployeesAdvertisementsCharts2Component } from './employees/advertisement-details/tapView/advertisements-charts2/advertisements-charts2.component';
import { EmployeesAdvertisementsCharts3Component } from './employees/advertisement-details/tapView/advertisements-charts3/advertisements-charts3.component';
import { EmployeesAdvertisementMapComponent      } from './employees/advertisement-details/tapView/advertisement-map/advertisement-map.component';

import { AdvertisementsRoutingModule      } from './advertisements-routing.module'
import { AdvertisementsService            } from './../../service/advertisements.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule             } from 'ngx-infinite-scroll';

import { GMapModule          } from 'primeng/gmap';
import { FileUploadModule    } from 'primeng/fileupload';
import { AutoCompleteModule  } from 'primeng/autocomplete';
import { InputTextModule     } from 'primeng/inputtext';
import { ButtonModule        } from 'primeng/button';
import { TooltipModule       } from 'primeng/tooltip';
import { DataGridModule      } from 'primeng/datagrid';
import { NgModule            } from '@angular/core';
import { CommonModule        } from '@angular/common';
import { PanelModule         } from 'primeng/panel';
import { ChartModule         } from 'primeng/chart';
import { CalendarModule      } from 'primeng/calendar';
import { TabViewModule       } from 'primeng/tabview';
import { DropdownModule      } from 'primeng/dropdown';
import { InputMaskModule     } from 'primeng/inputmask';
import { EditorModule        } from 'primeng/editor';

import { UsersAdvertisementDetailsComponent  } from './users/advertisement-details/advertisement-details.component';
import { UsersAdvertisementsChartsComponent  } from './users/advertisement-details/tapView/advertisements-charts/advertisements-charts.component';
import { UsersAdvertisementsCharts2Component } from './users/advertisement-details/tapView/advertisements-charts2/advertisements-charts2.component';
import { UsersAdvertisementsCharts3Component } from './users/advertisement-details/tapView/advertisements-charts3/advertisements-charts3.component';
import { UsersAdvertisementMapComponent      } from './users/advertisement-details/tapView/advertisement-map/advertisement-map.component';
import { UsersAdvertisementsComponent        } from './users/advertisements.component';


@NgModule({
  imports: [
    CommonModule,
    AdvertisementsRoutingModule,
    PanelModule,
    DataGridModule,
    FormsModule,
    TooltipModule,
    ButtonModule,
    InputTextModule,
    ChartModule,
    CalendarModule,
    TabViewModule,
    DropdownModule,
    AutoCompleteModule,
    InputMaskModule,
    ReactiveFormsModule,
    GMapModule,
    EditorModule,
    FileUploadModule,
    InfiniteScrollModule
  ],
  declarations: [

    // users
    UsersAdvertisementsComponent,
    UsersAdvertisementDetailsComponent,
    UsersAdvertisementsChartsComponent,
    UsersAdvertisementsCharts2Component,
    UsersAdvertisementsCharts3Component,
    UsersAdvertisementMapComponent,


    // employees
    EmployeesAdvertisementsComponent,
    EmployeesAdvertisementDetailsComponent,
    EmployeesAdvertisementsChartsComponent,
    EmployeesAdvertisementsCharts2Component,
    EmployeesAdvertisementsCharts3Component,
    EmployeesAdvertisementMapComponent,
    AddEmployeeAdvertisementComponent,

    
    AdvertisementsMapComponent
  ],
  providers : [
    AdvertisementsService
  ]
})
export class AdvertisementsModule { }
