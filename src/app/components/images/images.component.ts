import { ImagesService     } from './../../service/images.service';
import { Component, OnInit } from '@angular/core';

declare let $;

type ImageOnEventEmitter = {
  displayImages : boolean;
  imagesData : string[];
}


@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit {

  constructor(
    private imagesService: ImagesService
  ) { }

  public displayImages: boolean ;
  public imagesData: any[];


  private setOptions() {

    this.displayImages = false;
    this.imagesData = [];

  }

  public showImages(): void{

    setTimeout(() => {
      $( '.story-images' ).sliderPro();
    } , 100);
    
  }

  public ngOnInit() {

    this.setOptions();

    this.imagesService.images_occured.subscribe(  (res: ImageOnEventEmitter )  => {
      if( this.displayImages === true ) return;
      this.imagesData = res.imagesData;
      this.displayImages = res.displayImages;
      this.showImages();
    });

  }

}
