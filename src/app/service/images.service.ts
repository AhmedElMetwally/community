import { Injectable , EventEmitter } from '@angular/core';


type ImageOnEventEmitter = {
  displayImages : boolean;
  imagesData : string[];
}

@Injectable({
  providedIn: 'root'
})
export class ImagesService {
  
  constructor(){};
  
  public images_occured = new EventEmitter<ImageOnEventEmitter>();

  public emit( obj: ImageOnEventEmitter ) {
    this.images_occured.emit(obj)
  }

}
