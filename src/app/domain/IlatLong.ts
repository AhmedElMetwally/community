export interface IlatLong {
    latitude: number;
    longitude: number;
}