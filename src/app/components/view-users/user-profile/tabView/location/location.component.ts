import { LoadingService    } from './../../../../../service/loading.service';
import { ActivatedRoute    } from '@angular/router';
import { UsersService      } from './../../../../../service/user.service';
import { ErrorService      } from './../../../../../service/error.service';
import { Component, OnInit } from '@angular/core';

declare let google: any;

@Component({
  selector: 'app-user-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

  constructor(
    private errorService:   ErrorService,
    private route :         ActivatedRoute,
    private usersService:   UsersService,
    private loadingService: LoadingService,
  ) { };

  public options:  any;
  public overlays: any[];
  public id:       string;


  private getParams(): Promise<any> {
    return new Promise( resolve => {
      this.route
        .params
        .subscribe( params => {
          this.id = params.id;
          resolve()
        })
    })
  }

  private getUser() {

    this.usersService.getUser( this.id )
    .then(({ data }) => {

      let [ lat , long ] = data.result.profile.location.split(',');
      
      this.options = {
        center: {
          lat: parseFloat( lat ), 
          lng: parseFloat( long )
        },
        zoom: 8
      };
  
      this.overlays = [
        new google.maps.Marker({
          
          position: {
            lat: parseFloat( lat ), 
            lng: parseFloat( long )
          }, 
  
          title: data.result.profile.country
        })
      ];
    })
    .catch( e => this.errorService.showError( e ));

  }

  public async ngOnInit() {
    await this.getParams();
    await this.getUser();
  }

}
