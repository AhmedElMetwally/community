import { ErrorService     } from './../../service/error.service';
import { environment      } from './../../../environments/environment';
import { Iemployee        } from './../../domain/Iemployee';
import { EmployeesService } from './../../service/employee.service';
import { LoadingService   } from './../../service/loading.service';
import { Title            } from '@angular/platform-browser';
import { JwtHelperService } from '@auth0/angular-jwt';

import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit      } from '@angular/core';

const helper = new JwtHelperService();

declare let swal;

@Component({
  selector: 'app-my-employee-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  constructor (
    private route :           ActivatedRoute,
    private router :          Router,
    private titleService:     Title,
    private loadingService:   LoadingService,
    private employeesService: EmployeesService,
    private errorService:     ErrorService,
  ) { }
 
  public _id: string ;
  public employee: Iemployee ;
  public edit: any;

  public genders: any;
  public roles: any;
  public groups: any;

  public url: string;


  private setOptions(): void {

    this.url = environment.url;
    
    this.edit = {
      firstName : false,
      lastName  : false,
      mobileNumber: false,
      email     : false,
      password  : false,
      salary    : false,
      groups    : false,
      roles     : false,
    }

    this.groups = [
      {label: 'public official', value: 0 },
      {label: 'advertising officials', value: 1},
      {label: 'technical support officer', value: 2}
    ];

    this.genders = [
      {label: 'Male', value: 0},
      {label: 'Female', value: 1},
      {label: 'other', value: 2 }
    ];

    this.roles  = [
        {label:'Normal Employee', value: 'NORMAL_EMPL' },
        {label:'Super Employee', value:'SUPER_EMPL' },
    ];

  }

  private setTitle( ): void {
    this.titleService.setTitle( this.employee.firstName + ' ' + this.employee.lastName );
  }

  public addImage( event , form ): void {

    const formData: FormData = new FormData();
    formData.append('photo', event.files[0]  );
    formData.append('id', this._id );

    this.loadingService.show()
    
    this.employeesService.addMyImage( formData )
    .then( ({ data }) => {
      this.employee = data;
      this.loadingService.hide();
      form.clear();
    })
    .catch( error => this.errorService.showError( error ) )

  }

  public editEmployeeOnKeyUp( key , value , event ) {
    if( event.keyCode != 13 ) return;
    this.editEmployee(key , value)
  }

  public editEmployee( key , value ) {

    this.loadingService.show();

    // if(key === 'roles') value = value.indexOf('SUPER_EMPL') >= 0 ? ['SUPER_EMPL' , 'NORMAL_EMPL'] : ['NORMAL_EMPL'];
    // if(key === 'salary') value = parseFloat( value );


    let obj: any = {
      ...this.employee,
    };

    obj[ key ] = value;
    obj.id = this.employee._id;

    delete obj.createdAt;
    delete obj.updatedAt;
    delete obj._id;

    this.employeesService.editMyEmployee( obj )
    .then(() => {
      this.employee[ key ] = value;
      this.loadingService.hide();
      this.edit[ key ] = false;
    })
    .catch( error => this.errorService.showError(error))

  }

  public editEmployeePasswordOnKeyUp( password, event ) {
    if( event.keyCode != 13 ) return;
    this.editEmployeePassword(password )
  }

  public editEmployeePassword( password ) {

    this.loadingService.show();

    let obj: any = {
      ...this.employee,
    };

    obj['password'] = password;
    obj.id = this.employee._id;

    delete obj.createdAt;
    delete obj.updatedAt;
    delete obj._id;

    this.employeesService.editMyEmployeePassword( obj )
    .then(() => {

      this.edit['password'] = false;
      this.loadingService.hide();
    })
    .catch( error => this.errorService.showError(error))

  }

  private getEmployee(): Promise<any> {
    
    this.loadingService.show();

    return new Promise( resolve => {

      this._id = helper.decodeToken(localStorage.getItem('token'))._id

      this.employeesService.getEmployee( this._id )
      .then( ({data}) => {
        
        this.employee = data;
        
        this.setTitle();

        this.loadingService.hide();

        resolve();
      })
      .catch( error => this.errorService.showError( error ) )

    })
  }

  public getGroups( arr: string[] ) {
    enum groups {
        'public official' = 0,
        'advertising officials',
        'technical support officer',
    } 
    return arr.map( v => {
        return groups[v]
    }).join(' - ')
  }

  public getRoles( arr: string[] ) {
    enum roles {
      NORMAL_EMPL = 'Normal Employee',
      SUPER_EMPL = 'Super Employee' 
    } 
    return arr.map( v => {
        return roles[v]
    }).slice( 0 , 2 ).join(' - ')
  }

  public getGender( n: number): string {
    enum EnumGender {
      Male = 0,
      Female,
      other
    } 
    return EnumGender[n]
  }

  public Number( v ) {
    return parseInt( v );
  }

  public async ngOnInit() {
    this.setOptions();
    await this.getEmployee();
    
  }

}


