import { environment } from './../../environments/environment';
import { Injectable  } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable()
export class UsersService {

  constructor(
    private http: HttpClient
  ) { }
  private _headers:HttpHeaders = new HttpHeaders();
  private headers = this._headers
      .append('Content-Type', 'application/json')
      .append('Authorization', 'Bearer ' + localStorage.getItem('token'))
  ;

  private userUrl: string = environment.userUrl;


  // Users
  public getUsers ( country , page , limit , filterBy ): Promise<any> {
    
    let params = new HttpParams()
      .set('limit', limit)
      .set('page', page)
      .set('filterBy', filterBy);
    
    return this.http.get(
      `${this.userUrl}/api/v1/analytics/country/${country}`,
      {
        params, 
        headers : this.headers
      }
    ).toPromise();

  }
  public getUsersSearch(page , limit , number): Promise<any> {
    let params = new HttpParams()
      .set('limit', limit)
      .set('page', page)
      .set('number', number)
      .set('metadata', 'true')
    ;
    return this.http.get(
      `${this.userUrl}/api/v1/user/find/mobile`,
      {
        params, 
        headers : this.headers
      }
    ).toPromise()
  }


  // view User
  public getUser( id: string): Promise<any> {
    return this.http.get(
      `${this.userUrl}/api/v1/analytics/view/user/${id}`,
      {
        headers : this.headers
      }
    ).toPromise()
  }
  public uploadImage( formData: FormData , userToken: string ): Promise<any> {
    return this.http.post(
        `${this.userUrl}/api/v1/media/upload/photos`,
        formData,
        { 
            headers : new HttpHeaders()
            .append( 'Authorization', 'Bearer ' + userToken )
        }, 
    ).toPromise()
    .then( ( res: any ) => {
      return {
        profileImage : res.data[0].url,
        id           : res.data[0].id,
      }
    });
  }  
  public updateImageId( obj: any , userToken: string ): Promise<any> {
    return this.http.post(
        `${this.userUrl}/api/v1/user/update/profile-pic`,
        obj ,
        { 
            headers : new HttpHeaders()
            .append( 'Authorization', 'Bearer ' + userToken )
        }, 
    ).toPromise();
  }  
  public editUser( obj: any , userToken: string  ): Promise<any> {
    return this.http.post(
      `${this.userUrl}/api/v1/user/update`,
      obj,
      {
        headers : new HttpHeaders()
        .append( 'Authorization', 'Bearer ' + userToken )
      }
    ).toPromise()
  }
  public getContacts( userToken: string): Promise<any> {
    return this.http.get(
      `${this.userUrl}/api/v1/user/contacts/list`,
      { 
        headers : new HttpHeaders()
        .append( 'Authorization', 'Bearer ' + userToken )
    }, 
    ).toPromise()
  }
  public deleteContact( body , userToken ): Promise<any> {
    return fetch(
      `${this.userUrl}/api/v1/user/contacts/remove`,
      {
        body : JSON.stringify(body),
        method: 'Delete',
        headers: {
          "Content-Type": "application/json",
          "Authorization": 'Bearer ' + userToken,
        },
      }      
    )
  }
  public getStatuses( page , limit , includeReplies , type , id ,  userToken: string): Promise<any> {
    let params = new HttpParams()
      .set('limit', limit)
      .set('page', page)
      .set('includeReplies', includeReplies + '')
      .set('type', type)
      
    return this.http.get(
      `${this.userUrl}/api/v1/user/show/${id}/statuses`,
      { 
        params,
        headers : new HttpHeaders()
        .append( 'Authorization', 'Bearer ' + userToken )
    }, 
    ).toPromise()
  }
  public deleteStatuse( id , userToken ): Promise<any> {
    return fetch(
      `${this.userUrl}/api/v1/user/me/status/delete?id=${id}`,
      {
        method: 'Delete',
        headers: {
          "Content-Type": "application/json",
          "Authorization": 'Bearer ' + userToken,
        },
      }      
    )
  }
  public getStatics( id ): Promise<any> {
    return this.http.get(
      `${this.userUrl}/api/v1/analytics/view/user/${id}/statics`,
      { 
        headers : new HttpHeaders()
        .append( 'Authorization', 'Bearer ' + localStorage.getItem('token') )
    }, 
    ).toPromise()
  } 
  public blockAndUnBlockUser(id , isActive ): Promise<any>  {
    return this.http.post(
      `${this.userUrl}/api/v1/user/update/state`,
      {
        id , 
        isActive
      },
      {
        headers : this.headers
      }
    ).toPromise()
  }

  public getLikes( page , limit , id , userToken ): Promise<any> {
    
    let params = new HttpParams()
      .set('limit', limit)
      .set('page', page)
      .set('type', 'likes')
    ;

    return this.http.get(
      `${this.userUrl}/api/v1/user/view/status/${id}/actions`,
      { 
        params,
        headers : new HttpHeaders()
        .append( 'Authorization', 'Bearer ' + userToken )
      },
    ).toPromise();

  } 

  public getShares( page , limit , id , userToken ): Promise<any> {
    
    let params = new HttpParams()
      .set('limit', limit)
      .set('page', page)
    ;
    return this.http.get(
      `${this.userUrl}/api/v1/user/view/status/${id}/shares`,
      { 
        params,
        headers : new HttpHeaders()
        .append( 'Authorization', 'Bearer ' + userToken )
      },
    ).toPromise();

  } 

  public getReplies( page , limit , id , userToken ): Promise<any> {
    
    let params = new HttpParams()
      .set('limit', limit )
      .set('page', page)
    ;
    return this.http.get(
      `${this.userUrl}/api/v1/user/view/status/${id}/replies`,
      { 
        params,
        headers : new HttpHeaders()
        .append( 'Authorization', 'Bearer ' + userToken )
      },
    ).toPromise();

  } 




  // map
  public getUserStatics(): Promise<any> {
    return this.http.get(
      `${this.userUrl}/api/v1/analytics/global/statics`,
      {
        headers : this.headers
      }
    ).toPromise();
  }


  
  
}
