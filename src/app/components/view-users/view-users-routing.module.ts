import { UserProfileComponent } from './user-profile/user-profile.component';
import { ViewUsersComponent } from './view-users.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path : '',
    component : ViewUsersComponent,
  },
  {
    path : ':id',
    component : UserProfileComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewUsersRoutingModule { }
