import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MyProfileRoutingModule } from './my-profile-routing.module';
import { MyProfileComponent     } from './my-profile.component';
import { EmployeesService       } from './../../service/employee.service';

import { NgModule     } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DropdownModule    } from 'primeng/dropdown';
import { FileUploadModule  } from 'primeng/fileupload';
import { InputMaskModule   } from 'primeng/inputmask';
import { InputTextModule   } from 'primeng/inputtext';
import { TooltipModule     } from 'primeng/tooltip';
import { MultiSelectModule } from 'primeng/multiselect';

@NgModule({
  imports: [
    CommonModule,
    MyProfileRoutingModule,
    FileUploadModule,
    InputMaskModule,
    InputTextModule,
    TooltipModule,
    DropdownModule,
    MultiSelectModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    MyProfileComponent
  ],
  providers : [
    EmployeesService
  ]
})
export class MyProfileModule {}
