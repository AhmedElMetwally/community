export interface Iemployee {
    _id?: string;
    firstName: string;
    lastName: string;
    gender: number;
    email: string;
    mobileNumber: string;
    password: string;
    salary: number
    groups: string;
    roles: string[];
}