import { EmployeeChartsComponent     } from './employee-profile/tabView/employee-charts/employee-charts.component';
import { EmployeeStatisticsComponent } from './employee-profile/tabView/employee-statistics/employee-statistics.component';
import { EmployeeProfileComponent    } from './employee-profile/employee-profile.component';
import { AddEmployeeComponent        } from './add-employee/add-employee.component';
import { EmployeesComponent          } from './employees.component';

import { NgModule             } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path : '',
    component : EmployeesComponent
  },
  {
    path : 'add-employee',
    component : AddEmployeeComponent
  },
  {
    path : 'view/:_id',
    component : EmployeeProfileComponent
  },
  // {
  //   path : 'view/logs/:_id',
  //   component : EmployeeStatisticsComponent
  // },
  // {
  //   path : 'view/chart/:_id',
  //   component : EmployeeChartsComponent
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule { }
