import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersAroundWorldMapComponent } from './users-around-world-map.component';

const routes: Routes = [
  {
    path: '', 
    component: UsersAroundWorldMapComponent,
    data : { title : 'Users Around World Map'}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersAroundWorldMapRoutingModule { }
