import { environment } from './../../environments/environment';
import { Iemployee   } from './../domain/Iemployee';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable  } from '@angular/core';


@Injectable()
export class EmployeesService {

  constructor(
    private http: HttpClient
  ) { }
  private _headers:HttpHeaders = new HttpHeaders();
  private headers = this._headers
      .append('Content-Type', 'application/json')
      .append('Authorization', 'Bearer ' + localStorage.getItem('token'))
  ;

  private url: string = environment.url;


  // Employees
  public getEmployees(page , limit , filterBy ): Promise<any> {
    
    let params;
    
    if( filterBy === null ) {
      params = new HttpParams()
      .set('limit', limit)
      .set('page', page)
    } else {
      params = new HttpParams()
      .set('limit', limit)
      .set('page', page)
      .set('group', filterBy);
    }
    
    return this.http.get(
      `${this.url}/api/v1/employee/list`,
      {
        params, 
        headers : this.headers
      }
    ).toPromise();

  }
  public getEmployeesSearch(page , limit ,query): Promise<any> {
    let params = new HttpParams()
      .set('page', page)
      .set('limit', limit)
      .set('query', query)
    ;
    return this.http.get(
      `${this.url}/api/v1/employee/search`,
      {
        params, 
        headers : this.headers
      }
    ).toPromise()
  }


  // view Employee
  public addImage( formData: FormData ): Promise<any> {
    return this.http.post(
        `${this.url}/api/v1/employee/update-pic/any`,
        formData,
        { 
            headers : new HttpHeaders()
            .append('Authorization', 'Bearer ' + localStorage.getItem('token'))
        }, 
    ).toPromise();
  }
  public getEmployee(_id: string): Promise<any> {
    return this.http.get(
      `${this.url}/api/v1/employee/show/${_id}`,
      {
        headers : this.headers
      }
    ).toPromise()
  }
  public deleteEmployee(_id: string): Promise<any> {
    return this.http.delete(
      `${this.url}/api/v1/employee/delete/${_id}`,
      {
        headers : this.headers
      }
    ).toPromise()
  }
  public editEmployee( obj ): Promise<any> {
    return this.http.post(
      `${this.url}/api/v1/employee/update/any`,
      obj,
      {
        headers : this.headers
      }
    ).toPromise()
  }
  public editEmployeePassword( obj ): Promise<any> {
    return this.http.post(
      `${this.url}/api/v1/employee/update-password/any`,
      obj,
      {
        headers : this.headers
      }
    ).toPromise()
  }
  public getLogs(_id: string , page , limit , year? ): Promise<any> {
    let params;
    
    if( year === null ) {
      params = new HttpParams()
      .set('limit', limit)
      .set('page', page)
    } else {
      params = new HttpParams()
      .set('limit', limit)
      .set('page', page)
      .set('year', year);
    }
    return this.http.get(
      `${this.url}/api/v1/employee/show/${_id}/logs`,
      {
        params,
        headers : this.headers
      }
    ).toPromise()
  }
  public getComputedLogs(_id: string  ): Promise<any> {
    return this.http.get(
      `${this.url}/api/v1/employee/show/${_id}/computedlogs`,
      {
        headers : this.headers
      }
    ).toPromise()
  }
  
  



  // my profile
  public editMyEmployee( obj ): Promise<any> {
    return this.http.post(
      `${this.url}/api/v1/employee/update/me`,
      obj,
      {
        headers : this.headers
      }
    ).toPromise()
  }
  public editMyEmployeePassword( obj ): Promise<any> {
    return this.http.post(
      `${this.url}/api/v1/employee/update-password/me`,
      obj,
      {
        headers : this.headers
      }
    ).toPromise()
  }
  public addMyImage( formData: FormData ): Promise<any> {
    return this.http.post(
        `${this.url}/api/v1/employee/update-pic/me`,
        formData,
        { 
            headers : new HttpHeaders()
            .append('Authorization', 'Bearer ' + localStorage.getItem('token'))
        }, 
    ).toPromise();
  }

  // add
  public addEmployee( employee: Iemployee ): Promise<any> {
    return this.http.post(
      `${this.url}/api/v1/employee/create`,
      employee,
      {
        headers : this.headers
      }
    ).toPromise();
  }


  
  
}
