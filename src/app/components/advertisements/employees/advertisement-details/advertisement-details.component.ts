import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employees-advertisement-details',
  templateUrl: './advertisement-details.component.html',
  styleUrls: ['./advertisement-details.component.css']
})
export class EmployeesAdvertisementDetailsComponent implements OnInit {

  constructor(
    private route : ActivatedRoute,
    private titleService: Title,
  ) { }

  private advertisement_id: string ;
  
  public edit:any = {};



  public addImage( event , form ){
    
  }

  private setTitle( title: string): void {
    this.titleService.setTitle( title );
  }

  private getParam(): void {
    this.route.params.subscribe( params => {
        this.advertisement_id = params.advertisement_id;
    });
  }

  public ngOnInit() {
    this.getParam();
    this.setTitle( 'advertisement name' );
  }

}
