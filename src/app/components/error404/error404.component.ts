import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-error404',
  templateUrl: './error404.component.html',
  styleUrls: ['./error404.component.css']
})
export class Error404Component implements OnInit {

  constructor(
    private route: ActivatedRoute, 
    private titleService: Title, 
  ) { }



  private setTitle(): void {
    const title = this.route.snapshot.data['title'];
    this.titleService.setTitle( title );
  }

  ngOnInit() {
    this.setTitle();
  }

}
