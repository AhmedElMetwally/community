import { FormGroup, FormControl, Validators } from '@angular/forms';

import { LoadingService    } from './../../../../service/loading.service';
import { SelectItem        } from 'primeng/components/common/selectitem';
import { Title             } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { CountriesCode_Name as countries         } from './../../../users-around-world-map/countriescode-name';
import { AdvertisementsService } from './../../../../service/advertisements.service';

declare let $ , google , swal;


@Component({
  selector: 'app-add-employee-advertisement',
  templateUrl: './add-advertisement.component.html',
  styleUrls: ['./add-advertisement.component.css']
})
export class AddEmployeeAdvertisementComponent implements OnInit {

  constructor(
    private loadingService: LoadingService,
    private titleService:   Title,
    private advertisementsService: AdvertisementsService
  ){}

  public AddadvertisementForm: FormGroup;
 
  public mapOptions:  any;
  public mapOverlays: any[];
  public gmap:        any;

  public useGpsMood: boolean;

  public location:   number[]; // [long , lat ]

  public countries: any[];


  private initForm(): void {

    const checkPatternStr   = (c: FormControl) => /^[\w-\s]+$/g.test( c.value ) ? null : {
      invalid: true
    };

    const checkPatternNum   = (c: FormControl) => /^[\d\.]+$/g.test( c.value ) ? null : {
      invalid: true
    };

    this.AddadvertisementForm =  new FormGroup({ 
      name      : new FormControl( ''  , [ Validators.required  , checkPatternStr   ]),
      price     : new FormControl( ''  , [ Validators.required  , checkPatternNum   ]),
      distance  : new FormControl( ''  , [ Validators.required  , checkPatternNum   ]), 
      details   : new FormControl(  '' , ), 
    });

  } 
  
  private toTitleCase( str: string): string {
    let split =  str.replace( /[A-Z]/g , function( s: string ): any {
      return ' ' + s.toLowerCase() 
    });
    return split.replace(/\w\S*/g, function(txt: string){
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    })
  }

  public onSubmit(): void  {

    if( this.AddadvertisementForm.invalid) {
      for(let control in this.AddadvertisementForm.controls ) {

        if( this.AddadvertisementForm.controls[control].invalid ) {

          let _control = this.toTitleCase( control );

          if( this.AddadvertisementForm.controls[control].errors.required ) {
            swal( _control , `${_control} Is Required`  , "error");
          } else if ( this.AddadvertisementForm.controls[control].errors.invalid ) {
            swal( _control , `${_control} Is Invalid`  , "error");
          } 
          break;
          
        }

      }
      return
    }

    const advertisement = {
      name     : this.AddadvertisementForm.controls.name.value,
      price    : this.AddadvertisementForm.controls.price.value,
      distance : this.AddadvertisementForm.controls.distance.value,
      details  : this.AddadvertisementForm.controls.details.value,
      location : this.location 
    };
    console.log( advertisement )
    this.loadingService.show();
    setTimeout(() => {
      this.loadingService.hide();
    } , 1000)

  }

  public getErrorMsg( obj ): string {
    if(obj){
      if( obj.required) {
        return 'Required'
      } else if ( obj.invalid ){
        return 'Invalid'
      }
    }
  }

  public setTitle( title: string ): void {
    this.titleService.setTitle( title );
  };
 
  private setMapOptions() {
    let location = [30 , 30];
    this.mapOptions = {
      center: { 
        lat: location[1] , 
        lng: location[0]
      },
      zoom: 7
    };
    this.useGpsMood = false;
    this.location = [];
  }
 
  private getLocationByGps(): void {
    
    if( 'geolocation' in navigator ) {

      this.loadingService.show()
      
      navigator.geolocation
      .getCurrentPosition( 

        ( position: any ) => {
          
          this.useGpsMood = true;

          this.location = [position.coords.longitude ,position.coords.latitude];

          this.setMapCenter(this.location)

          this.addMarker(this.location);

          this.loadingService.hide()
        } , 
        
        ( error: any ) => {
          this.loadingService.hide();
          swal( 'GPS' , 'Can`t Get Location By GPS' , "error");
        } , {
          timeout: 5000,
        }

      );

    }

  }

  private setMapCenter( location ): void {
    this.gmap.setCenter(
      new google.maps.LatLng
      ( 
        location[1],  
        location[0]
      )
    )
    this.gmap.setZoom(5);
    this.addMarker(location);
    this.useGpsMood = true;
  }

  public handleMapClick( event ): void {
    if( ! this.useGpsMood ) {
      this.location = [ event.latLng.lng() , event.latLng.lat() ]
      this.addMarker( this.location )
    }
  }

  private addMarker(location) {
    this.mapOverlays = [];
    this.mapOverlays.push(
        new google.maps.Marker({
          position:{
            lat: location[1], 
            lng: location[0]
          }
      }));
  }

  public useMouseToGetLocation(): void {
    this.useGpsMood = false;
    this.mapOverlays = [];
    this.location = []  
  }

  private setOptions(): void {
    // this.countries = countries;
  }

  public get autoCompleteEvents(): any {
    const events: any = {};
    events.onSelect = (event): void => {
      this.advertisementsService.getLocationByCode( event.code )
      .then( ({latlng}) => {
        this.useMouseToGetLocation();
        this.location = latlng;
        this.setMapCenter(this.location);
      })
    };

    events.onClear = (): void => {
    };

    events.completeMethod = ( event ): void => {
      this.countries = [];
        for(let i = 0, len = countries.length ; i < len ; i++) {
            let CountryCode_Name = countries[i];
            if( CountryCode_Name.name.toLowerCase().indexOf(event.query.toLowerCase()) == 0 ) {
                this.countries.push(CountryCode_Name);
            }
        }
    }
    return events;
  }

  public setMap(event) {
    this.gmap = event.map;
  }

  public ngOnInit() {
    this.setTitle('Add Employee Advertisement');
    this.initForm();
    this.setMapOptions();
    this.setOptions();
  }


}
